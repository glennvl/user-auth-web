'use strict';

describe('Controller: PostalcodeCtrl', function () {

  // load the controller's module
  beforeEach(module('somWebApp'));

  var PostalcodeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    PostalcodeCtrl = $controller('PostalcodeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
