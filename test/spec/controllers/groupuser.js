'use strict';

describe('Controller: GroupuserCtrl', function () {

  // load the controller's module
  beforeEach(module('somWebApp'));

  var GroupuserCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    GroupuserCtrl = $controller('GroupuserCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
