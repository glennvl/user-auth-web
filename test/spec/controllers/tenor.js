'use strict';

describe('Controller: TenorCtrl', function () {

  // load the controller's module
  beforeEach(module('webAppApp'));

  var TenorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TenorCtrl = $controller('TenorCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
