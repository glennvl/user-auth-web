'use strict';

describe('Controller: ProvinceCtrl', function () {

  // load the controller's module
  beforeEach(module('somWebApp'));

  var ProvinceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ProvinceCtrl = $controller('ProvinceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
