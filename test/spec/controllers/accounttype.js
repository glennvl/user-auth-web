'use strict';

describe('Controller: AccounttypeCtrl', function () {

  // load the controller's module
  beforeEach(module('somWebApp'));

  var AccounttypeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AccounttypeCtrl = $controller('AccounttypeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
