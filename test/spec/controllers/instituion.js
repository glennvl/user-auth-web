'use strict';

describe('Controller: InstituionCtrl', function () {

  // load the controller's module
  beforeEach(module('somWebApp'));

  var InstituionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    InstituionCtrl = $controller('InstituionCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
