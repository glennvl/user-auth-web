'use strict';

angular.module('promoApp').service('restService', ['$resource', 'conf', function($resource, conf){
  this.getSimpleResource = function(requestMapping){
    return $resource(
      conf.http_host+conf.http_api+requestMapping+'/:entityId',
      {entityId: '@entityId'},{
        'save': {url:conf.http_host+conf.http_api+requestMapping, method: 'PUT'},
        'update': {url:conf.http_host+conf.http_api+requestMapping, method: 'PUT'},
        'find': {url:conf.http_host+conf.http_api+requestMapping,method: 'GET', isArray: false},
        'findOne': {method: 'GET'},
        'delete': {url:conf.http_host+conf.http_api+requestMapping, method: 'DELETE'},
        'download': {url:conf.http_host+conf.http_api+requestMapping, method: 'GET'},
        'listAll': {url:conf.http_host+conf.http_api+requestMapping,method: 'GET', isArray: true}
      }
    );
  };
  this.getVoucherResource = function(requestMapping){
    return $resource(
      conf.host_voucher+conf.api_voucher+requestMapping+'/:entityId',
      {entityId: '@entityId'},{
        'replace': {url:conf.host_voucher+conf.api_voucher+requestMapping, method: 'POST'},
        'generate': {url:conf.host_voucher+conf.api_voucher+requestMapping, method: 'POST'},
        'save': {url:conf.host_voucher+conf.api_voucher+requestMapping, method: 'PUT'},
        'update': {url:conf.host_voucher+conf.api_voucher+requestMapping, method: 'PUT'},
        'find': {url:conf.host_voucher+conf.api_voucher+requestMapping,method: 'GET', isArray: false},
        'findOne': {method: 'GET'},
        'delete': {url:conf.host_voucher+conf.api_voucher+requestMapping, method: 'DELETE'}
      }
    );
  };
  this.getUserResource = function(requestMapping){
    return $resource(
      conf.host_user+conf.api_user+requestMapping+'/:entityId',
      {entityId: '@entityId'},
      {
        'save': {url:conf.host_user+conf.api_user+requestMapping+"/save", method: 'POST'},
        'update': {url:conf.host_user+conf.api_user+requestMapping+"/update", method: 'POST'},
        'find': {url:conf.host_user+conf.api_user+requestMapping+"/find", method: 'GET', isArray: true},
        'findOne': {method: 'GET'},
        'delete': {url:conf.host_user+conf.api_user+requestMapping+"/delete", method: 'DELETE'},
        'findRoleTask': {url:conf.host_user+conf.api_user+requestMapping, method: 'GET', isArray: false},
        'assignTask': {url:conf.host_user+conf.api_user+requestMapping+"/assignTask", method: 'POST'},
        'changepwd': {url:conf.host_user+conf.api_user+requestMapping+"changepwd", method: 'POST'},
        'reset': {url:conf.host_user+conf.api_user+requestMapping+"/resetpwd", method: 'POST'},
        'logout': {url:conf.host_user+conf.api_user+"logout", method: 'GET'},
        'updateProfile': {url:conf.host_user+conf.api_user+"updateProfile", method: 'POST'}
      }
    );
  };
}]);