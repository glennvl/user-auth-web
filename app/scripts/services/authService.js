'use strict';
angular.module('promoApp')
.factory('authService', ['$http', '$q', 'localStorageService', 'conf',
  function ($http, $q, localStorageService, conf) {
  var serviceBase        = conf.apiServiceBaseUri;
  var authServiceFactory = {};
  var restUrl            = conf.host;
  var _authentication = {
    isAuth      : false
  };
  function calculatePasswordHash(password, username) {
    var hashedPass = CryptoJS.SHA256(password.toString()).toString(CryptoJS.enc.Hex);
    var secret = CryptoJS.SHA256(username).toString(CryptoJS.enc.Hex);
    var hash = CryptoJS.HmacSHA256(hashedPass, secret);
    var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
    return hashInBase64;
  };

  var responseParsing = function(dataInput){
    var dataParsed = {};
    var sortno = 1;
    var menuParsed = [];
    angular.forEach(dataInput.menus, function(value, key){
      if(value.isMenu){
        menuParsed.push({name: value.name, url: value.state, parent: value.parent, sortNo: sortno});
        sortno = sortno + 1;
      }
    });

    dataParsed = {  username: dataInput.username,
                    fullname: dataInput.firstName + dataInput.lastName,
                    email: dataInput.email,
                    phone: dataInput.phone,
                    roleCode: "SPA",
                    roleName: "SuperAdmin",
                    groupCode: "99999999999",
                    groupName: "Kartuku - Head Office",
                    parentCode: "99999999999",
                    parentName: "Kartuku - Head Office",
                    menu: menuParsed
                  };
                  // console.log(dataParsed);
    return dataParsed;
  }

  var _login = function(loginData){
        var tokenUrl = 'oauth/token?client_id=my-trusted-client-with-secret&client_secret=somesecret';
        var data = "grant_type=password&username=" + loginData.username + "&password=" + calculatePasswordHash(loginData.password, loginData.username);
        var deferred = $q.defer();
        $http.post(serviceBase+tokenUrl+"&"+data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
            localStorageService.set('authorizationData', { token: response.access_token, userName: loginData.username});
            _authentication.isAuth = true;
            _authentication.username = loginData.username;

            $http.get(serviceBase+"user/login?username="+loginData.username , { headers: { 'Content-Type': 'application/json' } }).success(function (data) {
              // console.log(data);
              localStorageService.set(
                  'voucherData', responseParsing(data)
              ); 
              deferred.resolve(responseParsing(data));
            }).error(function (err, status) {
                console.log(err)
            });
            // deferred.resolve(response);
        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });
        return deferred.promise;
  };
  var _logOut = function(){      
    localStorage.clear();
    _authentication.isAuth = false;
  };
  var _fillAuthData = function(){
    var authData = localStorageService.get('authorizationData');
    if(authData){
      _authentication.isAuth      = true;
      _authentication.username    = authData.username;
      _authentication.jsessionid  = authData.jsessionid;                     
    }
  };
  var _getVoucherData = function(){
    var authData = localStorageService.get('voucherData');
    return authData;
  }
  
  var _changePassword = function(user){
    var authData = localStorageService.get('authorizationData');
    user.valid = true;
    localStorageService.set('apiPath',{
       pathName : 'user/changepwd',
         
    });
    return $http.post(restUrl+conf.iam_v2, user);         
  };
    authServiceFactory.login          = _login;
    authServiceFactory.logOut         = _logOut;
    authServiceFactory.fillAuthData   = _fillAuthData;
    authServiceFactory.authentication = _authentication;
    authServiceFactory.changePassword = _changePassword;
    authServiceFactory.getVoucherData = _getVoucherData;
    return authServiceFactory;
  }
]);
