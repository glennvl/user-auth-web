﻿'use strict';
angular.module('promoApp').factory('authInterceptorService', ['$q', '$injector','$location', 'localStorageService', function ($q, $injector,$location, localStorageService) {

    var authInterceptorServiceFactory = {};
    var _request = function (config) {

    config.headers = config.headers || {};
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    };

    var _responseError = function (rejection, scope, Config, $window) {
        if (rejection.status === 401) {
            var authService = $injector.get('authService');
            var authData = localStorageService.get('authorizationData');

            if (authData) {
                if (authData.useRefreshTokens) {
                    $location.path('/refresh');
                    return $q.reject(rejection);
                }
            }
            // localStorage.clear();
            $location.path('/core/expire');
        }
        if (rejection.status === 403) {
            var authService = $injector.get('authService');
            var authData = localStorageService.get('authorizationData');
            if(authService.authentication.isAuth === false) {
                $location.path('/core/login');
            }else{
                //alert("session has expire!!!");
                // localStorage.clear();
                $location.path('/core/expire');
            }
        }
        if (rejection.status === 404) {
            $location.path('/core/page404');
        }
        if (rejection.status === 500) {
            //alert("internal server error!!!");
        }
        return $q.reject(rejection);
    };

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);
