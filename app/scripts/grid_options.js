'use strict';

angular.module('promoApp')
.controller('SimpleCrudCtrl', ['$scope', 'simpleResource', 'apiName', '$filter', '$controller', 'Msg', '$http', 'conf', '$q', function($scope, simpleResource, apiName, $filter, $controller, Msg, $http, conf, $q){
    $scope.sortInfo = {fields: [], columns: [], directions: []};
    $scope.defSortInfo = {fields: [],columns: [],directions: []};
    $scope.mySelections = [];
    $scope.filter = [];

    $scope.pagingOptions = {
        pageSizes: [10, 20, 30, 40, 50, 100],
        pageSize: 10,
        currentPage: 1
    };

    $scope.gridOptions = {
        data: 'data',
        columnDefs: $scope.columnDefs,
        enableRowSelection: true,
        enableColumnResizing: true,
        enableColumnMenus: false,
        showFooter: true,
        sortInfo: $scope.sortInfo,
        useExternalSorting: true,
        useExternalPagination: true,
        //enablePagination: true,
        // showGridFooter: true,
        gridFooterTemplate: 'views/table-template/dt-footer.html',
        onRegisterApi: registerGridApi,
        totalItems: $scope.totalServerItems,
        paginationOptions: $scope.pagingOptions,
        selectedItems: $scope.mySelections,
        enablePaginationControls: true,
        paginationPageSizes: [10, 20, 30, 50, 100],
        paginationPageSize: 10,
        minRowsToShow: 10,
        multiSelect: false,
        enableHorizontalScrollbar : 2,
        enableVerticalScrollbar : 2,
        enableGridMenu: true,
        exporterCsvFilename: 'file.csv',
        exporterMenuPdf: false,
        // exporterPdfDefaultStyle: {fontSize: 9},
        // exporterPdfTableStyle: {margin: [5, 5, 5, 5]},
        // exporterPdfTableHeaderStyle: {fontSize: 10, bold: true, italics: true, color: 'red'},
        // exporterPdfHeader: { text: "Header", style: 'headerStyle' },
        // exporterPdfFooter: function ( currentPage, pageCount ) {
        //   return { text: currentPage.toString() + ' of ' + pageCount.toString(), style: 'footerStyle' };
        // },
        // exporterPdfCustomFormatter: function ( docDefinition ) {
        //   docDefinition.styles.headerStyle = { fontSize: 22, bold: true }; 
        //   docDefinition.styles.footerStyle = { fontSize: 10, bold: true }; 
        //   return docDefinition;
        // },
        // exporterPdfOrientation: 'portrait',
        // exporterPdfPageSize: 'LETTER',
        // exporterPdfMaxGridWidth: 500,
        exporterCsvLinkElement: angular.element(document.querySelectorAll(".custom-csv-link-location"))
    };

    function registerGridApi(gridApi) {
      var selected = true;
      $scope.gridApi = gridApi;

      // if (gridApi.grid.options.columnDefs.length > 7)
      gridApi.grid.options.columnDefs[gridApi.grid.options.columnDefs.length - 1].headerCellClass = 'include-scrollbar';

      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length == 0) {
          $scope.pagingOptions.sort = null;
        } else {
          if(sortColumns[0].name==='username'||sortColumns[0].name==='lastSync') sortColumns[0].name = "userSyncList." + sortColumns[0].name;
          $scope.pagingOptions.sort = sortColumns[0].sort.direction;
          $scope.sortInfo.fields = [];
          $scope.sortInfo.fields.push(sortColumns[0].name);
          $scope.sortInfo.directions = [];
          $scope.sortInfo.directions.push(sortColumns[0].sort.direction);
        }
        $scope.find();
      });      

      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        $scope.pagingOptions.pageSize = pageSize;
        $scope.pagingOptions.currentPage = newPage;
        $scope.find();
      });

      gridApi.core.on.columnVisibilityChanged( $scope, function( changedColumn ){
        $scope.columnChanged = { name: changedColumn.colDef.name, visible: changedColumn.colDef.visible };
      });

      if(gridApi.selection){
        gridApi.selection.on.rowSelectionChanged($scope,function(rows){
          $scope.mySelections = gridApi.selection.getSelectedRows();
          $scope.getDetailRows($scope.mySelections, selected);
        });
      }
    }

    $scope.search = function() {
      $scope.pagingOptions.currentPage = 1;
      $scope.gridApi.pagination.seek(1);
      $scope.find();
    };

    $scope.refresh = function(){
      $scope.filter = {};
      $scope.search();
    };

    $scope.find = function() {
        var param = {};
        var strSort;
        var strSort = $scope.sortInfo.fields.length>0?$scope.buildSort($scope.sortInfo):$scope.buildSort($scope.defSortInfo);
        param.page = ($scope.pagingOptions.currentPage-1);//*$scope.pagingOptions.pageSize;
        param.size = $scope.pagingOptions.pageSize;

        if(strSort.length>0){
            param.sort = strSort;
        }

        for(var key in $scope.filter){
            var value = $scope.filter[key];
            if ((key === "clId") || (key === "installDateFrom") || (key === "installDateUntil")
              || (key === "midCreatedDateFrom") || (key === "midCreatedDateUntil")
              || (key === "tidCreatedDateFrom") || (key === "tidCreatedDateUntil")) {
              param[key] = value;
            }
            else if (key === "productRemark") {
              param[key] = value;
            } else if (typeof value === "string") {
              //param[key] = "%"+value+"%";

              if(value && value.length>0) param[key] = value;
            } else if (value instanceof Date) {
              param[key] = $filter('date')(value, "yyyy-MM-dd") + "T00:00:00";
            } else if (value instanceof Object) {
              if (Object.keys(value).length !== 0) {
                convertObjectToLinearParam(value, key, param);
              }
            } else {
              param[key] = value;
            }
          }


        var onSuccess = function(value, headers){
          $scope.viewGrid = true;

          $scope.data = value;
          $scope.gridApi.grid.options.totalItems = headers('page.total-elements');

        };

        var onError = function(error){
            var errorMessage = "Unknown";
            if(error.data.errorMessage){
                errorMessage = error.data.errorMessage;
                $scope.errorMessage = errorMessage;
            }else if(error.status===404){
                errorMessage = "Request URL not found.";
            }
            $scope.notificationError(Msg.error, "Cannot load data!");
            console.log("Error: "+errorMessage);
        };
        simpleResource.find(param, onSuccess, onError);
    };

    $scope.buildSort = function(sortInfo){
      var strSort = "";

      if(sortInfo.fields){
        var sortInfoLength = sortInfo.fields.length;

        for(var i=0; i<sortInfoLength; i++){
          strSort += sortInfo.fields[i];
          strSort += ",";
          strSort += sortInfo.directions[i] === "desc" ? "DESC" : "ASC";
          strSort += ",";
        }
        strSort = strSort?strSort.slice(0,-1):strSort;
      }
      return strSort;
    };

    var convertObjectToLinearParam = function(obj, paramName, param){
        if(obj instanceof Object){
            for(var key in obj){
                var nextParamName = new String(paramName+'.'+key);
                convertObjectToLinearParam(obj[key], nextParamName, param);
            }
        }else{
            if(typeof obj === "string"){
                if(obj.length!==0){
                    //param[paramName] = "%"+obj+"%";
                    console.log(obj);
                    param[paramName] = obj;
                }
            }else if(obj instanceof Date){
                param[paramName] = $filter('date')(obj, "yyyy-MM-dd");
            }
        }
    };
    
  }]);
