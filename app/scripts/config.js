'use strict';
var config = angular.module('promoApp');var login_title = "User Management";
var http_host = 'http://localhost:8083/';
var http_api = 'service-order/';
var host_voucher = 'http://172.30.10.154:11080/';
var api_voucher = 'VOUCHER-WEB-API/';
var serviceBase = http_host+http_api;
var promo_version = "01";

// var host_voucher = 'http://localhost:8080/';
// var api_voucher = 'VOUCHER-WEB-API/';

//LOCAL CONFIG
// var http_login = "http://172.30.10.154:11080/login-rest/login";
// var host_user = "http://172.30.10.154:11080/";
// var api_user = "login-rest/";
// var host_voucher = 'http://localhost:8080/';
// var api_voucher = 'VOUCHER-WEB-API/';

//DEVELOPMENT CONFIG
var http_login = "http://localhost:9001/#/login";
var host_user = 'http://localhost:8083/';
var api_user = 'service-order/';
var host_voucher = 'http://172.30.10.154:11080/';
var api_voucher = 'VOUCHER-WEB-API/';

//STAGING CONFIG
// var http_login = "https://evoucher-dev.kartuku.co.id/AUTH-LDAP-API/login";
// var host_user = "https://evoucher-dev.kartuku.co.id/";
// var api_user = "AUTH-LDAP-API/";
// var host_voucher = 'https://evoucher-dev.kartuku.co.id/';
// var api_voucher = 'VOUCHER-WEB-API/';

config.constant('conf', {
  'root': '/',
  'login_title': login_title,
  'http_host': http_host,
  'http_api': http_api,
  'host_voucher': host_voucher,
  'api_voucher': api_voucher,
  'promo_version': promo_version,
  'host_user': host_user,
  'api_user': api_user,
  'http_login': http_login,
  apiServiceBaseUri: serviceBase,
  clientId: 'ngAuthApp'
});