'use strict';
angular.module('promoApp')
.controller('RoleCtrl', function($scope,$http,$controller,$location,restService,rsc,conf,Msg,$modal,authService,code){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.role, simpleResource: restService.getUserResource(rsc.role)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.role, simpleResource: restService.getUserResource(rsc.role)});
  $controller('CheckerCtrl', {$scope: $scope});
  $scope.list = {};
  $scope.groupParent = authService.getVoucherData().groupCode;
  var isSuperAdmin = $scope.groupParent == code.superadmin;
  $scope.isAdmin = isSuperAdmin;
  if(!isSuperAdmin){
    $scope.filter.parent = $scope.groupParent;
  }
  $scope.page = { title: 'Role Management', main: 'Cardlink' };
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.gridOptions.columnDefs = [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field:'extra1', displayName:'Action', width:100, cellTemplate: 'views/administration/roles/_action-column.html', enableSorting: false, headerCellClass: "center"},
    {field:'extra2', displayName:'Assign', width:75, cellTemplate: 'views/administration/roles/_assign-role.html', enableSorting: false, headerCellClass: "center"},
    {field:'roleCode', displayName: "Code", enableSorting: false},
    {field:'roleName', displayName: "Name", enableSorting: false},
    {field:'groupName', displayName: "Group", enableSorting: false}
  ];
  $scope.find();
  $scope.reload = function(){
    $scope.filter = {};
    if(!isSuperAdmin){
      $scope.filter.parent = $scope.groupParent;
    };
    $scope.find();
  };
  var getGroup = function(){
    var Group = restService.getUserResource(rsc.group);
    Group.find(!isSuperAdmin ? {parent: $scope.groupParent}:"").$promise.then(function (data) {
      $scope.list.group = data.content;
      if(isSuperAdmin){
        var sup = {groupCode: "-", groupName: "Group Parent"};
        $scope.list.group.unshift(sup);
      }
    },
    function (error) {
        $scope.notificationError(Msg.error, Msg.failed_get_data);
    });
  };
  getGroup();
  $scope.create = function(item) {
    $scope.items = []; 
    $scope.update(item,'myModalContent', '', $scope.group);
  };
  $scope.detail = function(items){
    $scope.openDetail(items);
  };

  $scope.open_role_group = function(row) {
    var modalInstance = $modal.open({
      templateUrl: 'roleGroup.html',
      controller: 'RoleGroupCtrl',
      resolve: {
        rows: function () {
          return row;
        },
        scope: function(){
          return $scope;
        }
      }
    });
  };
})
.controller('RoleGroupCtrl', function ($scope, scope, $modalInstance, rows, restService, $controller, Msg, rsc) {
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.role, simpleResource: restService.getUserResource(rsc.role)});
  $controller('ToasterDemoCtrl', {$scope: $scope});
  $scope.isAdmin = scope.isAdmin;
  $scope.items = rows;
  $scope.arr = [];
  $scope.crud = ["create","read","update","delete","approve"];
  $scope.viewby = 10;
  $scope.currentPage = 1;
  $scope.itemsPerPage = $scope.viewby;
  $scope.maxSize = 10;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.setItemsPerPage = function(num) {
    $scope.itemsPerPage = num;
    $scope.currentPage = 1; //reset to first page
  };
  $scope.getBoolean = function(val){
    if(val == 'true') return true;
    else if(val == 'disable') return val;
    return false;
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

  function uniqueBy(arr, fn) {
    var unique = {};
    var distinct = [];
    arr.forEach(function (x) {
      var key = fn(x);
      if (!unique[key]) {
        distinct.push({parent: key});
        unique[key] = true;
      }
    });
    return distinct;
  };
  $scope.getIndex = function(obj){
    var index;
    if(obj.task.split("_")[1] == 'create') index = 0;
    if(obj.task.split("_")[1] == 'read') index = 1;
    if(obj.task.split("_")[1] == 'update') index = 2;
    if(obj.task.split("_")[1] == 'delete') index = 3;
    if(obj.task.split("_")[1] == 'approve') index = 4;
    return index;
  }
  $scope.getRoleTask = function(code){
    var roles = restService.getUserResource(rsc.role+"/getRoleTask");
    roles.findRoleTask({roleCode: code}).$promise.then(function (data) {
      $scope.menus = uniqueBy(data.content, function(x){
       return x.menu;
      });
      $scope.items.menus = $scope.menus;
      $scope.items.roles = parsingRole(data.content);
      $scope.totalItems = $scope.menus.length;
    },
    function (error) {
        $scope.notificationError(Msg.error, Msg.failed_get_data);
    });
  };
  $scope.getRoleTask(rows.roleCode);

  function parsingRole(val){
    var obj={}; var arr= [];
    obj.role = []; obj.group = []; obj.user = []; obj.company = []; obj.voucher_generate = []; obj.voucher_detail = []; obj.transaction_log = []; obj.voucher_replace = []; obj.voucher_type = [];
    angular.forEach(val, function(val, key){
      if(val.menu === rsc.company){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.company.push(val);
      } else if(val.menu === rsc.parsing_voucher_generate){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.voucher_generate.push(val);
      } else if(val.menu === rsc.voucher_detail){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.voucher_detail.push(val);
      }else if(val.menu === rsc.transaction_log){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.transaction_log.push(val);
      }else if(val.menu === rsc.parsing_voucher_replace){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.voucher_replace.push(val);
      }else if(val.menu === rsc.voucher_type){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.voucher_type.push(val);
      }else if(val.menu === "user"){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.user.push(val);
      }else if(val.menu === "group"){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.group.push(val);
      }else if(val.menu === "role"){
        val.status = $scope.getBoolean(val.status);
        val.sort = $scope.getIndex(val);
        obj.role.push(val);
      }
    });
    arr.push({name: 'Company', code: 0, val: obj.company});
    arr.push({name: 'Voucher Generate', code: 1, val: obj.voucher_generate});
    arr.push({name: 'Voucher Detail', code: 2, val: obj.voucher_detail});
    arr.push({name: 'Transaction Log', code: 3, val: obj.transaction_log});
    arr.push({name: 'Voucher Transfer', code: 4, val: obj.voucher_replace});
    arr.push({name: 'Voucher Type', code: 5, val: obj.voucher_type});
    arr.push({name: 'Group', code: 6, val: obj.group});
    arr.push({name: 'User', code: 7, val: obj.user});
    arr.push({name: 'Role', code: 8, val: obj.role});
    return arr;
  };

  $scope.assign_role = function(item){
    $scope.assignRole(item);
    $modalInstance.dismiss('cancel');
  }
});