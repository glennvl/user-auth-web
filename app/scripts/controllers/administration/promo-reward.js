'use strict';

angular.module('promoApp')
.controller('PromoRewardCtrl', function($scope,$http,$controller,$location,restService,rsc){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.promo, simpleResource: restService.getSimpleResource(rsc.promo)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.promo, simpleResource: restService.getSimpleResource(rsc.promo)});
  $scope.page = {
    title: 'Promo Reward',
    main: 'Promo'
  };
  $scope.defSortInfo = {fields: ['name'], directions: ['asc']};
  $scope.statusList = [{id:0, name:"BLOCKED"}, {id:1, name:"ACTIVATED"}];
  $scope.gridOptions.columnDefs = [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field:'extra', displayName:'Action', width:100, cellTemplate: 'views/administration/company/action-column.html', enableSorting: false, headerCellClass: "center"},
    {field:'name', displayName: "Promo Name"},
    {field:'processor', displayName: "Promo Processor"},
    {field:'status', displayName: "Status", cellTemplate:'<div class="getStatus" my-data="{{COL_FIELD}}"></div>'},
    {field:'createdDate', displayName: "Created Date", cellFilter: "date: 'yyyy-MM-dd HH:mm:ss'"},
    {field:'lastUpdatedDate', displayName: "Last Update", cellFilter: "date: 'yyyy-MM-dd HH:mm:ss'"}
  ];
  $scope.find();

    $scope.create = function(item) {
      item = {createdDate:new Date(), status:1};
      $scope.update(item);
    };

    $scope.edit = function(item) {
      var itemUpdated = angular.copy(item);
      itemUpdated.lastUpdatedDate = new Date();
      $scope.update(itemUpdated);
    };

    $scope.changeStatus = function(item){
      var itemUpdated = angular.copy(item);
      if(itemUpdated.status === 0){
        itemUpdated.lastUpdatedDate = new Date();
        itemUpdated.status = 1;
      }else{
        itemUpdated.lastUpdatedDate = new Date();
        itemUpdated.status = 0;
      }

      $scope.save(itemUpdated);
    }
});