'use strict';
angular.module('promoApp')
.controller('VoucherTypeCtrl', function($scope,$http,$controller,$location,restService, conf, Msg, $q, localStorageService, rsc){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher_type, simpleResource: restService.getVoucherResource(rsc.voucher_type)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.voucher_type, simpleResource: restService.getVoucherResource(rsc.voucher_type)});
  $controller('CheckerCtrl', {$scope: $scope});
  $controller('CommonCtrl', {$scope: $scope});
  $scope.list = {};
  $scope.page = {title: 'Voucher Type Management',main: 'Cardlink'};
  $scope.list.redeemPriorityList = [{id:1, name:"Expire date ascending, amount descending"}, {id:2, name:"Amount ascending, expire date descending"}];
  $scope.list.expiredUnitList = [{id:0, name:"DATE"}, {id:1, name:"DAYS"}, {id:2, name:"WEEKS"}, {id:3, name:"MONTHS"},{id:4, name:"YEARS"}];
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.userData = localStorageService.get('voucherData');
  $scope.gridOptions.columnDefs = [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field: 'extra1', displayName: 'Action', width: 100, cellTemplate: 'views/administration/voucher-type/_action-column.html', headerCellClass: "center", enableSorting: false},
    {field:'id', displayName: "Code", width: 150},
    {field:'label', displayName: "Name", width: 150},
    {field:'companies[0].name', displayName: "Company", width: 150},
    // {field:'description', displayName: "Description"},
    {field:'minAmount', displayName: "Min Amount", cellFilter: 'currencyFilter', width: 120},
    {field:'maxAmount', displayName: "Max Amount", cellFilter: 'currencyFilter', width: 120},
    // {field:'maxVoucherUse', displayName: "Max Voucher"},
    // {field:'redeemPriorityDescription', displayName: "Redeem Priority"},
    // {field:'expiredUnitType', displayName: "Expired Type"},
    // {field:'merchantSpecific', displayName: "Merchant Specific"},
    // {field:'daySpecific', displayName: "Day Specific"},
    {field:'createBy', displayName: "Created By", width: 120},            
    {field:'createDate', displayName: "Created Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150},
    {field:'changeBy', displayName: "Updated By", width: 120},   
    {field:'changeDate', displayName: "Last Updated", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150}
  ];
  $scope.find();
  $scope.getAllCompany();

  $scope.create = function(item) {
    item = {createBy : $scope.userData.username, createDate:new Date(), disableCode:false, companies:[]};
    $scope.update(item);
  };

  $scope.edit = function(item) {
    var itemUpdated = angular.copy(item);
    itemUpdated.changeBy = $scope.userData.username;
    itemUpdated.changeDate = new Date();
    itemUpdated.disableCode = true;
    $scope.update(itemUpdated);
  };

  $scope.delete = function(item){
    if(item){
      var deletedItems = [];
      deletedItems.push(item);
      $http({url: conf.host_voucher+conf.api_voucher+rsc.voucher_type, method: 'DELETE', data: deletedItems, headers: {"Content-Type": "application/json;charset=utf-8"}}).then(
        function(){
          $scope.find();
          $scope.notificationSuccess(Msg.success, Msg.data_berhasil_dihapus);
        },
        function() {
          $scope.find();
          $scope.notificationError(Msg.error, Msg.data_gagal_dihapus);
        }
      )
    }
  };
});