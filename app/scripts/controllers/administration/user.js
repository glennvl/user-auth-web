'use strict';
angular.module('promoApp')
.controller('UserCtrl', function($scope,$http,$controller,$location,restService,rsc,conf,Msg,authService,code){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.user, simpleResource: restService.getUserResource(rsc.user)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.user, simpleResource: restService.getUserResource(rsc.user)});
  $controller('CheckerCtrl', {$scope: $scope});
  $scope.list = {};
  $scope.groupParent = authService.getVoucherData().groupCode;
  var isSuperAdmin = $scope.groupParent == code.superadmin;
  if(!isSuperAdmin){
    $scope.filter.parent = $scope.groupParent;
  }
  $scope.page = { title: 'User Management', main: 'Cardlink' };
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.gridOptions.columnDefs = [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field:'extra1', displayName:'Action', width:100, cellTemplate: 'views/administration/users/_action-column.html', enableSorting: false, headerCellClass: "center"},
    {field:'extra2', displayName:'Reset', width:100, cellTemplate: 'views/administration/users/_action-column-reset.html', enableSorting: false, headerCellClass: "center"},
    {field:'username', displayName: "Username", enableSorting: false},
    {field:'lastName', displayName: "Last Name", enableSorting: false},
    {field:'email', displayName: "Email", enableSorting: false},
    {field:'phone', displayName: "Phone", enableSorting: false}
  ];
  $scope.find();
  $scope.reload = function(){
    $scope.filter = {};
    if(!isSuperAdmin){
      $scope.filter.parent = $scope.groupParent;
    };
    $scope.find();
  };
  $scope.getRoles = function(){
    var roles = restService.getUserResource(rsc.role);
    roles.find(!isSuperAdmin ? {parent: $scope.groupParent}:"").$promise.then(function (data) {
      $scope.list.roles = data;
    },
    function (error) {
        $scope.notificationError(Msg.error, Msg.failed_get_data);
    });
  };
  $scope.getRoles();
  $scope.detail = function(items){
    $scope.openDetail(items);
  }
  $scope.create = function(item) {
    $scope.items = []; 
    $scope.update(item);
  };
  $scope.reset = function(item) {
   $scope.resetPassword(item);
  };
});