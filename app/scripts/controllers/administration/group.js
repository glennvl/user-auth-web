'use strict';
angular.module('promoApp')
.controller('GroupCtrl', function($scope,$http,$controller,$location,restService,rsc,conf,Msg,authService,code){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.group, simpleResource: restService.getUserResource(rsc.group)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.group, simpleResource: restService.getUserResource(rsc.group)});
  $controller('CheckerCtrl', {$scope: $scope});
  $scope.page = { title: 'Group Management', main: 'Cardlink' };
  $scope.list = {};
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.groupParent = authService.getVoucherData().groupCode;
  var isSuperAdmin = $scope.groupParent == code.superadmin;
  if(!isSuperAdmin){
    $scope.filter.parent = $scope.groupParent;
  }
  $scope.gridOptions.columnDefs = [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field:'extra', displayName:'Action', width:100, cellTemplate: 'views/administration/groups/_action-column.html', enableSorting: false, headerCellClass: "center", enableHiding: true},
    {field:'groupCode', displayName: "Group Code", enableSorting: false},
    {field:'groupName', displayName: "Group Name", enableSorting: false},
    {field:'groupParent', displayName: "Group Parent", enableSorting: false}
  ];
  $scope.find();

  $scope.reload = function(){
    $scope.filter = {};
    if(!isSuperAdmin){
      $scope.filter.parent = $scope.groupParent;
    };
    $scope.find();
  }
  var getGroup = function(){
    var Group = restService.getUserResource(rsc.group);
    Group.find(!isSuperAdmin ? {parent: $scope.groupParent}:"").$promise.then(function (data) {
      $scope.list.group = data.content;
      if(isSuperAdmin){
        var sup = {groupCode: "-", groupName: "Group Parent"};
        $scope.list.group.unshift(sup);
      }
    },
    function (error) {
        $scope.notificationError(Msg.error, Msg.failed_get_data);
    });
  };
  getGroup();
  $scope.detail = function(items){
    $scope.openDetail(items);
  }
  $scope.create = function(item) {
    $scope.items = []; 
    $scope.update(item);
  };
});