'use strict';

angular.module('promoApp')
  .controller('MerchantGroupCtrl', function($scope,$http,$controller,$location,restService,rsc,$modal){
    $controller('SimpleCrudCtrl', {$scope: $scope, apiName: "group-merchant", simpleResource: restService.getSimpleResource("group-merchant")});
    $controller('CrudCtrl', {$scope: $scope, apiName: rsc.company, simpleResource: restService.getSimpleResource(rsc.company)});
    $scope.page = {
      title: 'Manajemen Merchant Group',
      main: 'Promo'
    };

    $scope.gridOptions.columnDefs =
    [ 
      {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
      {field:'merchant.mid', displayName: "MID"},       
      {field:'groups.name', displayName: "Group Name"},
    ];
    $scope.find();

   $scope.list = {};

    $scope.findAll(rsc.group).then(function(obj){
      $scope.list.groups = obj.data.content;
    });

    $scope.findAll(rsc.merchant).then(function(obj){
      $scope.list.merchants = obj.data.content;
    });

    $scope.open_group_form = function(row) {
      var modalInstance = $modal.open({
        templateUrl: '_form-group.html',
        controller: 'GroupCtrl',
        backdrop: 'static',
        resolve: {
          scope: function () {
            return $scope;
          }
        }
      });
    };

    $scope.open_merchant_form = function(row) {
      var modalInstance = $modal.open({
        templateUrl: '_form-merchant.html',
        controller: 'MerchantCtrl',
        backdrop: 'static',
        resolve: {
          scope: function () {
            return $scope;
          }
        }
      });
    };

  })

  .controller('GroupCtrl', function ($scope, $modalInstance, restService, $controller, rsc) {
    $controller('CrudCtrl', {$scope: $scope, apiName: rsc.company, simpleResource: restService.getSimpleResource(rsc.company)});
    
    $scope.ok = function (items) {
      $scope.save(items);
      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })

  .controller('MerchantCtrl', function ($scope, $modalInstance, restService, $controller, rsc) {
    $controller('CrudCtrl', {$scope: $scope, apiName: rsc.merchant, simpleResource: restService.getSimpleResource(rsc.merchant)});
    
    $scope.ok = function (items) {
      $scope.save(items);
      $modalInstance.close();
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  });
