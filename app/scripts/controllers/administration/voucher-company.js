'use strict';

angular.module('promoApp')
  .controller('VoucherCompanyCtrl', function($scope,$http,$controller,$location,restService,rsc, conf, Msg, localStorageService, $q){
    $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.company, simpleResource: restService.getVoucherResource(rsc.company)});
    $controller('CrudCtrl', {$scope: $scope, apiName: rsc.company, simpleResource: restService.getVoucherResource(rsc.company)});
    $controller('CheckerCtrl', {$scope: $scope});

    $scope.page = {title: 'Company Management',main: 'Cardlink'};
    $scope.defSortInfo = {fields: ['name'], directions: ['asc']};
    $scope.userData = localStorageService.get('voucherData');
    $scope.gridOptions.columnDefs =
    [ 
      {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
      {field:'extra', displayName:'Action', width:100, cellTemplate: 'views/administration/voucher-company/_action-column.html', enableSorting: false, headerCellClass: "center"},
      {field:'name', displayName: "Name", enableSorting: false},
      {field:'code', displayName: "Code", enableSorting: false}, 
      {field:'createBy', displayName: "Created By", enableSorting: false},            
      {field:'createDate', displayName: "Created Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", enableSorting: false},
      {field:'changeBy', displayName: "Updated By", enableSorting: false},   
      {field:'changeDate', displayName: "Last Updated", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", enableSorting: false}
    ];
    $scope.find();

    $scope.create = function(item) {
      item = {createBy : $scope.userData.username, createDate:new Date(),changeBy : $scope.userData.username, changeDate:new Date(), disableCode:false};
      $scope.update(item);
    };

    $scope.edit = function(item) {
      var itemUpdated = angular.copy(item);
      itemUpdated.changeBy = $scope.userData.username;
      itemUpdated.changeDate = new Date();
      itemUpdated.disableCode = true;
      $scope.update(itemUpdated);
    };

    $scope.delete = function(item){
      if(item){
        var deletedItems = [];
        deletedItems.push(item);
        $http({url: conf.host_voucher+conf.api_voucher+rsc.company, method: 'DELETE', data: deletedItems, headers: {"Content-Type": "application/json;charset=utf-8"}}).then(
          function(){
            $scope.find();
            $scope.notificationSuccess(Msg.success, Msg.data_berhasil_dihapus);
          },
          function() {
            $scope.find();
            $scope.notificationError(Msg.error, Msg.data_gagal_dihapus);
          }
        )
      }
    };


  });