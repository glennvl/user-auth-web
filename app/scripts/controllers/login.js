'use strict';

angular.module('promoApp')
  .controller('LoginCtrl', function ($scope, $state, $location, authService, $modal, conf, $controller) {
    $scope.promo_version = conf.promo_version;
    $scope.user = {
        username: "",
        password: "",
        useRefreshTokens: false
    };
    localStorage.clear();
    $scope.message = "";

    $scope.login = function () {
      authService.login($scope.user).then(function (response) {
        var checker = ifChecker(response, "voucher-replace_approve");
        if(checker) {
          $location.path('/app/voucher-replace');
        }else{
          $location.path('/app/dashboard');
        };
      },
      function (err) {
        if(!err.message){
         $scope.message = "Invalid Username or Password";
        }else{
         $scope.message = err.message;
        }
      });
    };

    var ifChecker = function(data, task){
      var access =  false;
      angular.forEach(data.task, function(value, key){
        if(value.code.includes(task)){
         access = true;
         return access;
        }
      })
      return access;
    }

    $scope.open_forgot_password_modal = function() {
      var modalInstance = $modal.open({
        templateUrl: 'forgotpassmodal.html',
        controller: 'forgotpassCtrl',
      });
    };
  })
  
  .controller('forgotpassCtrl', function ($scope, $modalInstance) {
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

  });
