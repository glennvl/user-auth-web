'use strict';

angular.module('promoApp')
  .controller('ListCampaignCtrl', function($scope,$http,$controller,$location,restService,rsc){
    $controller('SimpleCrudCtrl', {$scope: $scope, apiName: "promo-manager", simpleResource: restService.getSimpleResource("promo-manager")});
    $controller('CrudCtrl', {$scope: $scope, apiName: "promo-manager", simpleResource: restService.getSimpleResource("promo-manager")});
    $scope.page = {
      title: 'List Campaign',
      main: 'Promo'
    };
    $scope.gridOptions.columnDefs = [ 
      {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
      {field:'label', displayName: 'Promo Label', width: 200},
      {field:"startDate | date:'dd-MM-yyyy HH:mm:ss'", displayName: 'Start Date', width: 150},
      {field:"endDate | date:'dd-MM-yyyy HH:mm:ss'", displayName: 'End Date', width: 150},
      {field:'promo.name', displayName: 'Promo Processor', width: 200},
      {field:'company.name', displayName: 'Company Name', width: 150},
      {field:'groups.name', displayName: 'Group Name', width: 150},
      //{field:'cappingDailyLimit', displayName: 'Daily Limit', width: 100},
      //{field:'cappingMonthlyLimit', displayName: 'Monthly Limit', width: 100},
      {field:'promoDay', displayName: 'Promo Day', width: 100, cellTemplate:"<div class='getFlag' my-data='{{row.getProperty(col.field)}}'></div>"},    
      //{field:'promoLimitFlag', displayName: 'Promo Limit', width: 100, cellTemplate:"<div class='getFlag' my-data='{{row.getProperty(col.field)}}'></div>"},
      {field:'processNextPromo', displayName: 'Multi Promo', width: 100, cellTemplate:"<div class='getFlag' my-data='{{row.getProperty(col.field)}}'></div>"},
      {field:'status', displayName: 'Status', width: 100, cellTemplate:"<div class='getStatus' my-data='{{row.getProperty(col.field)}}'></div>"}
    ];
    $scope.find();
  });
