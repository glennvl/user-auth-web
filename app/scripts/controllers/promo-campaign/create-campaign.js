'use strict';

angular.module('promoApp')
  .controller('CreateCampaignCtrl', function($scope,$http,$controller,$location,restService,rsc){
    $controller('CrudCtrl', {$scope: $scope, apiName: rsc.company, simpleResource: restService.getSimpleResource(rsc.company)});
    $scope.page = {
      title: 'Create Campaign',
      main: 'Promo'
    };
    $scope.list = {};
    $scope.findAll(rsc.company).then(function(obj){
      $scope.list.companies = obj.data.content;
    })
    $scope.binRangeList = [{'no':1,'start':'','end':''}];
    $scope.items = {};

    $scope.addBinRange = function() {
      var newItemNo = $scope.binRangeList.length+1;
      $scope.binRangeList.push({'no':newItemNo});
    };

    $scope.removeBinRange = function() {
      var lastItem = $scope.binRangeList.length-1;
      $scope.binRangeList.splice(lastItem);
    };
  });
