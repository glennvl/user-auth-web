'use strict';

angular.module('promoApp')
.controller('NavCtrl', function ($scope, localStorageService, $rootScope, $translate) {
  $scope.oneAtATime = false;
  var dashboard = {code: "homepage", "name": "HomePage", parent: "Homepage", url: "app.dashboard"};
  var pageTitleTranslationId = 'PAGE_TITLE';
  var pageContentTranslationId = 'PAGE_CONTENT';
  $scope.status = {
    isFirstOpen: true,
    isSecondOpen: true,
    isThirdOpen: true
  };
  var data = localStorageService.get('voucherData');
  $scope.menu = data.menu;
  $scope.menu.push(dashboard);
  $scope.menu.reverse();
  var fa = ["home", "magic", "credit-card"];
  angular.forEach($scope.menu, function(value, key){
    value.fa = fa[key];
  })
  $scope.submenu = $scope.menu;

  function uniqueBy(arr, fn) {
    var unique = {};
    var distinct = [];
    arr.forEach(function (x) {
      var key = fn(x);
      if (!unique[key]) {
        distinct.push({parent: key, fa: fa[distinct.length]});
        unique[key] = true;
      }
    });
    return distinct;
  }
  $scope.menus = uniqueBy($scope.menu, function(x){return x.parent;});

  $translate(pageTitleTranslationId, pageContentTranslationId)
  .then(function (translatedPageTitle, translatedPageContent) {
    $rootScope.pageTitle = translatedPageTitle;
    $rootScope.pageContent = translatedPageContent;
  });

  $rootScope.$on('$routeChangeSuccess', function (event, current) {
    $scope.currentPath = current.$$route.originalPath;
  });

  $scope.locale = $translate.use();

  $rootScope.$on('$translateChangeSuccess', function (event, data) {
    $scope.locale = data.language;
    $rootScope.pageTitle = $translate.instant(pageTitleTranslationId);
    $rootScope.pageContent = $translate.instant(pageContentTranslationId);
  });

});
