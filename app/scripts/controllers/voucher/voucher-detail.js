'use strict';

angular.module('promoApp')
.controller('VoucherDetailCtrl', function($scope, $controller, $filter, restService, rsc, authService, code, localStorageService, listStatus){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher, simpleResource: restService.getVoucherResource(rsc.voucher)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.voucher, simpleResource: restService.getVoucherResource(rsc.voucher)});
  $controller('CommonCtrl', {$scope: $scope});
  $controller('CheckerCtrl', {$scope: $scope});
  $scope.page = { title: 'Voucher Detail', main: 'Cardlink' };
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.userData = localStorageService.get('voucherData');
  $scope.groupParent = authService.getVoucherData().parentCode;
  $scope.isAdmin = $scope.groupParent == code.superadmin;
  if(!$scope.isAdmin){
    $scope.filter.companyCode = $scope.groupParent;
  }
  $scope.visibleColumn = $scope.buttonAccess('voucher-detail_update');
  $scope.list = listStatus.voucherDetail;
  $scope.gridOptions.columnDefs =
  [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    // {field: 'extra1', displayName: 'View', width: 75, cellTemplate: 'views/action-column-detail.html', headerCellClass: "center", enableSorting: false},
    {field: 'extra2', displayName: 'Block/Unblock', width: 120, cellTemplate: 'views/action-column-block.html', headerCellClass: "center", enableSorting: false, visible: $scope.visibleColumn},
    {field:'voucherTypeLabel', displayName: "Voucher Type"},
    {field:'amount', displayName: "Amount", cellFilter: 'currencyFilter'},
    {field:'expiredDate', displayName: "Expired Date", cellFilter: "date: 'dd-MM-yyyy'"},
    {field:'statusId', displayName: "Status", cellTemplate:'<div class="getVoucherStatus" my-data="{{COL_FIELD}}"></div>'},
    {field:'createBy', displayName: "Created By"},
    {field:'createDate', displayName: "Created Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150},
    {field:'changeBy', displayName: "Updated By"},
    {field:'changeDate', displayName: "Updated Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150}
  ];

  $scope.find();
  $scope.getAllVoucherType();
  $scope.getAllCompany();

  $scope.create = function(item) {
    $scope.items = []; $scope.update(item);
  };

  $scope.searchBy = function(cat, filter){
    if(cat == "pan"){
      $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher+"/link", simpleResource: restService.getVoucherResource(rsc.voucher+"/link")});
      $scope.filter = filter;
      $scope.search();
    }else if(cat == "category"){
      $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher, simpleResource: restService.getVoucherResource(rsc.voucher)});
      $scope.filter = filter;
      $scope.search();
    }else if(cat == "token"){
      $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher+"/token", simpleResource: restService.getVoucherResource(rsc.voucher+"/token")});
      $scope.filter = filter;
      $scope.search();
    }
  };

  $scope.refreshList = function(){
    $scope.filter = {};
    $("#searchForm").find('option:selected').removeAttr('selected').trigger('chosen:updated');
    if(!$scope.isAdmin){
      $scope.filter.companyCode = $scope.userData.parentCode;
    }
    $scope.search();
  };
    
  $scope.blockUnblock = function(items){
    var itemUpdated = angular.copy(items);
    itemUpdated.changeBy = $scope.userData.username;
    itemUpdated.changeDate = new Date();

    if(items.statusId == 2){
      itemUpdated.statusId = 1;
    }else if(items.statusId == 1){
      itemUpdated.statusId = 2;
    }
    $scope.block(itemUpdated);
  }
});