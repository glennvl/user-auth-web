'use strict';

angular.module('promoApp')
.controller('TransactionLogCtrl', function($scope,$http,$controller,$location,$modal,restService,rsc,localStorageService,authService,code,txnTypes,field,lable){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.txn_log, simpleResource: restService.getVoucherResource(rsc.txn_log)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.txn_log, simpleResource: restService.getVoucherResource(rsc.txn_log)});
  $controller('CommonCtrl', {$scope: $scope});
  $controller('CheckerCtrl', {$scope: $scope});
  $scope.page = {title: 'Transaction Log',main: 'Cardlink'};
  $scope.defSortInfo = {fields: ['id'], directions: ['asc']};
  $scope.userData = localStorageService.get('voucherData');
  $scope.parentCode = authService.getVoucherData().parentCode;
  $scope.isSuperAdmin = $scope.parentCode == code.superadmin;
  if(!$scope.isSuperAdmin){
    $scope.filter.companyCode = $scope.parentCode;
  }
  $scope.list = {};
  $scope.txnTypes = txnTypes;
  $scope.gridOptions.columnDefs =
  [ 
    {field: field.extra, displayName: lable.no, width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field: field.extra1, displayName: lable.view, width: 75, cellTemplate: 'views/action-column-detail.html', headerCellClass: "center", enableSorting: false},
    {field: field.txnReqDate, displayName: lable.transaction_date, cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150},
    {field: field.txnTypeType, displayName: lable.transaction_type, width: 150, cellTemplate: '<div style="text-align: center;" ng-init="value = grid.appScope.getValueTrxType(row.entity.txnTypeType)">{{value}}</div>'},
    {field: field.pan, displayName: lable.token, width: 175},
    {field: field.maskedPan, displayName: lable.masking_pan, width: 150},
    {field: field.companyCode, displayName: lable.company_code, width: 150},
    {field: field.tid, displayName: lable.tid, width: 100},
    {field: field.mid, displayName: lable.mid, width: 150},
    {field: field.invoice, displayName: lable.invoice_number, width: 150},
    {field: field.originalAmount, displayName: lable.original_amount, cellFilter: 'currencyFilter', width: 150},
    {field: field.salesAmount, displayName: lable.sales_amount, cellFilter: 'currencyFilter', width: 150},
    {field: field.voucherAmount, displayName: lable.voucher_amount, cellFilter: 'currencyFilter', width: 150},
    {field: field.responseCode, displayName: lable.response_code, width: 150},
    {field: field.responseDetail, displayName: lable.response_detail, width: 150},
    {field: field.additionalInfo, displayName: lable.additional_info, width: 200}
  ];
  $scope.find();
  $scope.getAllCompany();
  $scope.refreshList = function(){
    $scope.filter = {};
    $("#searchForm").find('option:selected').removeAttr('selected').trigger('chosen:updated');
    if(!$scope.isSuperAdmin){
      $scope.filter.companyCode = $scope.userData.parentCode;
    }
    $scope.search();
  };
  $scope.detail = function(items){
    $scope.openDetail(items);
  };
  function getTxnLogDetail(items) {
    var filter = {id:items.id};
    filter.sort ='name';
    restService.getVoucherResource(rsc.company).find(filter).$promise.then(function (result) {
      $scope.companyList = result.content;
    });
  };
  $scope.getValueTrxType = function(val){
    var arr = val.split(" ");
    var res = arr.slice(-1)[0];
    return res;
  } 
});
