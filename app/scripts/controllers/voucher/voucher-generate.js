'use strict';

angular.module('promoApp')
.controller('VoucherGenerateCtrl', function($scope, $http, $controller, $location, restService, rsc, localStorageService, conf, Msg, $q, $modal, authService, code){
  $controller('SimpleCrudCtrl', {$scope: $scope, apiName: rsc.voucher_generate_request, simpleResource: restService.getVoucherResource(rsc.voucher_generate_request)});
  $controller('CrudCtrl', {$scope: $scope, apiName: rsc.voucher_generate_request, simpleResource: restService.getVoucherResource(rsc.voucher_generate_request)});
  $controller('CheckerCtrl', {$scope: $scope});
  $controller('CommonCtrl', {$scope: $scope});
  $controller('ToasterDemoCtrl', {$scope: $scope});
  $scope.page = { title: 'Voucher Generate Request Form', main: 'Cardlink' };
  $scope.userData = localStorageService.get('voucherData');
  $scope.checker = $scope.buttonAccess('voucher-generate_approve');
  $scope.list = {'status' : [{'code':1, 'name':'DRAFT'}, {'code':2, 'name':'REVIEW'}, {'code':3, 'name':'APPROVED'}, {'code':4, 'name':'REJECTED'}]};
  $scope.defSortInfo = {fields: ['status', 'id'], directions: ['asc', 'asc']};
  // $scope.filter = {};
  $scope.parentCode = $scope.userData.parentCode;
  var isSuperAdmin = $scope.parentCode == code.superadmin;
  if(!isSuperAdmin){
    $scope.filter.companyCode = $scope.parentCode;
  }
  if($scope.checker) {
    // $scope.filter.approveBy = $scope.userData.username;
    $scope.filter.status = 2;
  }else{
    $scope.filter.createBy = $scope.userData.username;
  }
  $scope.gridOptions.columnDefs =
  [ 
    {field: 'extra', displayName: "No", width: 50, cellTemplate: "<div class='ui-grid-cell-contents'>{{grid.renderContainers.body.visibleRowCache.indexOf(row)+((grid.appScope.pagingOptions.currentPage-1)*grid.appScope.pagingOptions.pageSize)+1}}</a>", enableSorting: false},
    {field: 'extra1', displayName: 'Action', width: 120, cellTemplate: 'views/action-column-custom.html', headerCellClass: "center", enableSorting: false},
    {field:'pan', displayName: "PAN", width: 200},
    {field:'voucherTypeLabel', displayName: "Type", width: 200},
    {field:'amount', displayName: "Amount", width: 150, cellFilter: 'currencyFilter'},
    {field:'quantity', displayName: "Qty", width: 75},
    {field:'expiredDate', displayName: "Expired Date", cellFilter: "date: 'dd-MM-yyyy'", width: 100},
    {field:'status', displayName: "Status", width: 100, cellTemplate:'<div class="getVoucherGenerateStatus" my-data="{{COL_FIELD}}"></div>'},
    {field:'remark', displayName: "Remark", width: 300},
    {field:'rejectRemark', displayName: "Reject Remark", width: 300},
    {field:'createBy', displayName: "Created By", width: 150},            
    {field:'createDate', displayName: "Created Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150},
    {field:'approveBy', displayName: "Approved By", width: 150},   
    {field:'approveDate', displayName: "Approved Date", cellFilter: "date: 'dd-MM-yyyy HH:mm:ss'", width: 150}
  ];
  $scope.find();
  $scope.getAllVoucherType();

  $scope.create = function(item) {
    item = {createBy : $scope.userData.username, createDate:new Date(), status:1};
    $scope.update(item);
  };

  $scope.edit = function(item) {
    var itemUpdated = angular.copy(item);
    itemUpdated.changeBy = $scope.userData.username;
    itemUpdated.changeDate = new Date();
    $scope.update(itemUpdated);
  };

  $scope.delete = function(item){
    if(item){
      var deletedItems = [];
      deletedItems.push(item);
      $http({url: conf.host_voucher+conf.api_voucher+rsc.voucher_generate_request, method: 'DELETE', data: deletedItems, headers: {"Content-Type": "application/json;charset=utf-8"}}).then(
        function(){
          $scope.find();
          $scope.notificationSuccess(Msg.success, Msg.data_berhasil_dihapus);
        },
        function() {
          $scope.find();
          $scope.notificationError(Msg.error, Msg.data_gagal_dihapus);
        }
      )
    }
  };

  $scope.refresh = function(){
    $scope.filter = {};
    $("#searchForm").find('option:selected').removeAttr('selected').trigger('chosen:updated');
    if($scope.checker) {
      $scope.filter.status = 2;
    }else{
      $scope.filter.createBy = $scope.userData.username;
      $scope.filter.status = 1;
    }
    if(!isSuperAdmin){
      $scope.filter.companyCode = $scope.userData.parentCode;
    }
    $scope.search();
  }

  $scope.submit = function(item) {
    var itemUpdated = angular.copy(item);
    itemUpdated.changeBy = $scope.userData.username;
    itemUpdated.changeDate = new Date();
    itemUpdated.status = 2;
    $scope.save(itemUpdated);
  }

  $scope.approved = function(item){
    var deferred = $q.defer();
    item.approveBy = $scope.userData.username;
    item.approveDate = new Date();
    item.status = 3;
    restService.getVoucherResource(rsc.voucher_generate).generate(item).$promise.then(function (result) {
      $scope.notificationSuccess(Msg.success, Msg.data_berhasil_disimpan);
      $scope.find();
    },
    function (error) {
        deferred.reject(error);
        $scope.find();
        if(error.data) $scope.notificationError(Msg.error, error.data.error);
          else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    });
  }

  $scope.reject = function(item){
    var itemUpdated = angular.copy(item);
    itemUpdated.changeBy = $scope.userData.username;
    itemUpdated.changeDate = new Date();
    itemUpdated.status = 4;
    $scope.update(itemUpdated, "_form-reject");
  }

  $scope.getVoucherGenerateChecker = function(status){
    $scope.pagingOptions.currentPage = 1;
    $scope.gridApi.pagination.seek(1);
    if(!$scope.checker) {
      $scope.filter.approveBy = $scope.userData.username;
    }
    $scope.filter.status = status;
    // $scope.filter.companyCode = $scope.userData.parentCode;
    var isSuperAdmin = $scope.parentCode == code.superadmin;
    if(!isSuperAdmin){
      $scope.filter.companyCode = $scope.parentCode;
    }
    $scope.gridOptions.columnDefs;
    $scope.find();
  }

});