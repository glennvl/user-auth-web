'use strict';

angular.module('promoApp')
  .controller('HeaderCtrl', function ($scope, $location, authService) {
       $scope.logOut = function () {
        authService.logOut();
        $location.path('/');
       };
  });
