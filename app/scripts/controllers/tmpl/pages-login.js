'use strict';

angular.module('promoApp')
  .controller('LoginCtrl', function ($scope, $state, $location, authService) {
    // $scope.login = function() {
    //   $state.go('app.dashboard');
    // };

    $scope.user = {
        username: "",
        password: "",
        useRefreshTokens: false
    };

    $scope.message = "";

    $scope.login = function () {
        authService.login($scope.user).then(function (response) {
            // $scope.$root.$broadcast("onLoginSuccess");
            // var cssMenu = document.getElementById("cssmenu");
            // if(cssMenu !== null) {
            //     cssMenu.style.display = "block";
            // }
            $location.path('/app/dashboard');
        },
         function (err) {
             $scope.message = "Invalid Username or Password";
         });
    };

  });
