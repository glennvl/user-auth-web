// 'use strict';

// angular.module('promoApp')
//   .controller('ModalsCtrl', function ($scope) {
//     $scope.page = {
//       title: 'Modals',
//       subtitle: 'Place subtitle here...'
//     };
//   })

//   .controller('ModalDemoCtrl', function ($scope, $modal, $log) {

//     $scope.open = function(templateUrl, size) {

//       var modalInstance = $modal.open({
//         backdrop: 'static',
//         size: size,
//         templateUrl: templateUrl+'.html',
//         controller: 'ModalInstanceCtrl',
//         //size: size,
//         resolve: {
//           items: function () {
//             return $scope.items;
//           },
//           list: function () {
//             return $scope.list;
//           },
//           find: function () {
//             return $scope.find;
//           },
//           save: function () {
//             return $scope.save;
//           },
//           searchPOT: function () {
//             return $scope.searchPOT;
//           }
//         }
//       });

//       modalInstance.result.then(function (selectedItem) {
//         $scope.selected = selectedItem;
//       }, function () {
//         //$log.info('Modal dismissed at: ' + new Date());
//       });
//     };
//   })

//   .controller('ModalInstanceCtrl', function ($scope, $modalInstance, items, $controller, list, save) {
//     $controller('ToasterDemoCtrl', {$scope: $scope});
//     // $controller('CommonCtrl', {$scope: $scope});

//     $scope.available = false;
//     $scope.cityAvailable = false;
//     $scope.items = items;
//     $scope.list = list;

//     $scope.filter = {};

//     $scope.selected = {
//       item: $scope.items[0]
//     };

//     $scope.addInput = function (name) {
//       $scope.items[0][name].push("");
//     };


//     $scope.ok = function () {
//       if(items[0].variables) constructWorkflowVariable();
//       $scope.save(items[0]);
//       $modalInstance.close($scope.selected.item);
//     };

//     $scope.cancel = function () {
//       $scope.items = [];
//       $modalInstance.dismiss('cancel');
//     };

//     $scope.addInput = function(grp){
//       $scope.items[0].grp.push({
//         "id": ""
//       });
//     };
//   $scope.save = save;
//   })

//   .controller('SplashDemoCtrl', function ($scope, $modal, $log) {
//     $scope.items = ['item1', 'item2', 'item3'];

//     $scope.openSplash = function(event, size) {

//       var options = angular.element(event.target).data('options');

//       var modalInstance = $modal.open({
//         templateUrl: 'mySplashContent.html',
//         controller: 'ModalInstanceCtrl',
//         size: 'lg',
//         backdropClass: 'splash' + ' ' + options,
//         windowClass: 'splash' + ' ' + options,
//         resolve: {
//           items: function () {
//             return $scope.items;
//           }
//         }
//       });

//       modalInstance.result.then(function (selectedItem) {
//         $scope.selected = selectedItem;
//       }, function () {
//         $log.info('Modal dismissed at: ' + new Date());
//       });
//     };
//   });

