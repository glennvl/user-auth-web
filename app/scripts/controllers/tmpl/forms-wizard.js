'use strict';

/**
 * @ngdoc function
 * @name promoApp.controller:FormsWizardCtrl
 * @description
 * # FormsWizardCtrl
 * Controller of the promoApp
 */
angular.module('promoApp')
  .controller('FormWizardCtrl', function ($scope) {
    $scope.page = {
      title: 'Form Wizard',
      subtitle: 'Place subtitle here...'
    };
  });
