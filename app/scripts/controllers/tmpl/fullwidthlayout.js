'use strict';

/**
 * @ngdoc function
 * @name promoApp.controller:FullwidthlayoutCtrl
 * @description
 * # FullwidthlayoutCtrl
 * Controller of the promoApp
 */
angular.module('promoApp')
  .controller('FullwidthlayoutCtrl', function ($scope) {
    $scope.page = {
      title: 'Full-width Layout',
      subtitle: 'Place subtitle here...'
    };
  });
