'use strict';

angular.module('promoApp')
  .controller('MainCtrl', function ($scope, $controller, $location, authService, restService, localStorageService, $window, conf, $modal, $log) {
    $scope.webTitle = conf.login_title;
    $scope.users = localStorageService.get('voucherData');
    $scope.logOut = function() {
      restService.getUserResource().logout().$promise.then(function () { 
        authService.logOut();
        $location.path('/login');
      }, function(err){
        authService.logOut();
        $location.path('/login');
      });
    };

    $scope.page = {
      logout: 'Logout'
    };

    $scope.main = {
      title: 'Home',
      settings: {
        navbarHeaderColor: 'scheme-default',
        sidebarColor: 'scheme-default',
        brandingColor: 'scheme-default',
        activeColor: 'lightred-scheme-color',
        headerFixed: true,
        asideFixed: true,
        rightbarShow: false
      }
    };

    $scope.open_user_profile = function() {
      var modalInstance = $modal.open({
        templateUrl: 'userProfile.html',
        controller: 'UserProfileCtrl',
        resolve: {
          users: function () {
            return $scope.users;
          },
          groups: function () {
            return $scope.groups;
          }
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

    $scope.open_change_password = function() {
      var modalInstance = $modal.open({
        templateUrl: 'changePassword.html',
        controller: 'ChangePassCtrl',
        resolve: {
          scope: function () {
            return $scope
          }
        }
      });
      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };

  })

  .controller('UserProfileCtrl', function ($scope, $modalInstance, users, title, Msg, restService, $controller) {
    $controller('ToasterDemoCtrl', {$scope: $scope});
    $scope.users = users;
    $scope.editMode = true;
    $scope.txtBtn = title.btn.edit;
    $scope.setFalse = function(){
      $scope.editMode = false;
      $scope.txtBtn = title.btn.save;
    };
    $scope.update = function(item){
      var Profile = restService.getUserResource();
      Profile.updateProfile(item).$promise.then(function (data) {
        $scope.editMode = true;
        $scope.txtBtn = title.btn.edit;
        $scope.notificationSuccess(Msg.success, Msg.success_update_profile);
        $modalInstance.close();
      },
      function (error) {
          $scope.notificationError(Msg.error, Msg.data_gagal_diubah);
      });
    };
    $scope.ok = function () {
      $scope.save($scope.users);
      $modalInstance.close();
    };
    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };
  })

  .controller('ChangePassCtrl', function (scope, $scope, $modalInstance, toastr, $controller, rsc, restService) {
    $controller('CrudCtrl', {$scope: $scope, apiName: '', simpleResource: restService.getUserResource('')});
    $scope.items = {};
    $scope.items.username = scope.users.username;
    $scope.change_pass = function (item) {
      if(item){
        item.oldPassword = calculatePasswordHash(item.oldPassword, item.username);
        item.newPassword = calculatePasswordHash(item.newPassword, item.username);
      }
      $scope.changePass(item);
      $modalInstance.close($scope.item);
    };

    $scope.cancel = function () {
      $modalInstance.dismiss('cancel');
    };

    function calculatePasswordHash(password, username) {
      var hashedPass = CryptoJS.SHA256(password.toString()).toString(CryptoJS.enc.Hex);
      var secret = CryptoJS.SHA256(username).toString(CryptoJS.enc.Hex);
      var hash = CryptoJS.HmacSHA256(hashedPass, secret);
      var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
      return hashInBase64;
    };

  });
