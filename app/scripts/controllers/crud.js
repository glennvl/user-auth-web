'use strict';

angular.module('promoApp')
.controller('CrudCtrl', ['$scope', 'simpleResource', 'apiName', '$filter', '$controller', 'Msg', '$http', 'conf', '$q', 'localStorageService', function($scope, simpleResource, apiName, $filter, $controller, Msg, $http, conf, $q, localStorageService){
  $controller('ModalDemoCtrl', {$scope: $scope, simpleResource: simpleResource});
  $controller('ToasterDemoCtrl', {$scope: $scope});

  $scope.openDetail = function(items){
    $scope.viewDetail(items);
  }
  $scope.create = function(templateUrl, size, item){
    $scope.items = [];
    $scope.update(item, templateUrl, size);
  }
  $scope.update = function(item, templateUrl, size, list){
    $scope.items = [];
    if(item) item.editMode = true;
    $scope.items.push(angular.copy(item));
    if(templateUrl == null) templateUrl = 'myModalContent';
    $scope.open(templateUrl, size, list);
  };
  $scope.save = function(item){
    var deferred = $q.defer();
    if(item) {
      var savedItem = []
      savedItem.push(angular.copy(item));
      
      simpleResource.save(savedItem).$promise.then(function () {
        $scope.notificationSuccess(Msg.success, Msg.data_berhasil_disimpan);
        if(item.refresh)$scope.refresh();
        else $scope.find();
      },
      function (error) {
          deferred.reject(error);
          if(error.data) $scope.notificationError(Msg.error, error.data.error);
            else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
      });
    }
    return deferred.promise;
  };

  $scope.createItem = function(item){
    var deferred = $q.defer();
    if(!item.editMode) {
      simpleResource.save(item).$promise.then(function (response) {
        if(response.code == 1){
          $scope.notificationSuccess(Msg.success, response.message);
        }else{
          $scope.notificationError(Msg.error, response.message);
        }
        if(item.refresh)$scope.refresh();
        else $scope.find();
      },
      function (error) {
          deferred.reject(error);
          if(error.data) $scope.notificationError(Msg.error, error.data.message);
            else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
      });
    }else{
      simpleResource.save(item).$promise.then(function (response) {
        $scope.notificationSuccess(Msg.success, Msg.data_berhasil_disimpan);
        if(item.refresh)$scope.refresh();
        else $scope.find();
      },
      function (error) {
          deferred.reject(error);
          if(error.data) $scope.notificationError(Msg.error, error.data.message);
            else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
      });
    }
    return deferred.promise;
  };

  $scope.block = function(items){
    var deferred = $q.defer();
    simpleResource.update([items]).$promise.then(function (result) {
      $scope.find();
      $scope.notificationSuccess(Msg.success, Msg.data_berhasil_disimpan);
    },
    function (error) {
        deferred.reject(error);
        if(error.data) $scope.notificationError(Msg.error, error.data.message);
          else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    });
    return deferred.promise;
  }

  $scope.assignRole = function(item){
    var deferred = $q.defer();
    var obj = {}; var log = [];
    obj.roleCode = item.roleCode;
    for(var x=0;x<item.roles.length;x++){
      log = log.concat(item.roles[x].val);
    }
    obj.roleTaskList = log;
    simpleResource.assignTask(obj).$promise.then(function (response) {
      $scope.notificationSuccess(Msg.success, Msg.data_berhasil_disimpan);
    },
    function (error) {
        deferred.reject(error);
        if(error.data) $scope.notificationError(Msg.error, error.data.message);
          else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    });
    return deferred.promise;
  };

  $scope.changePass = function(item){
    var deferred = $q.defer();
    simpleResource.changepwd(item).$promise.then(function (response) {
      if(response.code == 1){
        $scope.notificationSuccess(Msg.success, response.message);
      }else{
        $scope.notificationError(Msg.error, response.message);
      }
    },
    function (error) {
        deferred.reject(error);
        if(error.data) $scope.notificationError(Msg.error, error.data.message);
          else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    });
    return deferred.promise;
  };

  $scope.resetPassword = function(item){
    var deferred = $q.defer();
    simpleResource.reset(item).$promise.then(function (response) {
      if(response.code == 1){
        $scope.notificationSuccess(Msg.success, response.message);
      }else{
        $scope.notificationError(Msg.error, response.message);
      }
    },
    function (error) {
        deferred.reject(error);
        if(error.data) $scope.notificationError(Msg.error, error.data.message);
          else $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    });
    return deferred.promise;
  };

  $scope.delete = function(item){
    // if(item){
    //   var deletedItems = [];
    //   deletedItems.push(item);
    //   $http({url: conf.host_voucher+conf.api_voucher+apiName, method: 'DELETE', data: deletedItems, headers: {"Content-Type": "application/json;charset=utf-8"}}).then(
    //     function(){
    //       $scope.find();
    //       $scope.notificationSuccess(Msg.success, Msg.data_berhasil_dihapus);
    //     },
    //     function() {
    //       $scope.find();
    //       $scope.notificationError(Msg.error, Msg.data_gagal_dihapus);
    //     }
    //   )
    // }
    var deferred = $q.defer();
    if(item) {
      simpleResource.delete(item).$promise.then(function () {
        $scope.notificationSuccess(Msg.success, Msg.data_berhasil_dihapus);
        if(item.refresh)$scope.refresh();
        else $scope.find();
      },
      function (error) {
          deferred.reject(error);
          if(error.data) $scope.notificationError(Msg.error, error.data.message);
            else $scope.notificationError(Msg.error, Msg.data_gagal_dihapus);
      });
    }
    return deferred.promise;
  };

  $scope.findAll = function(resource){
    return $http({url: conf.http_host+conf.http_api+resource, method: 'GET', headers: {"Content-Type": "application/json;charset=utf-8"}});
    // simpleResource.find().$promise.then(function (data) {
    //   console.log(data);
    // },
    // function (error) {
    //     $scope.notificationError(Msg.error, Msg.data_gagal_disimpan);
    // });
  };

  $scope.filterDate = function(date){
    return $filter('date')(date, 'yyyy-MM-dd');
  };

  $scope.filterStartDate = function(date){
    return $filter('date')(date, 'yyyy-MM-dd 00:00:00');
  };

  $scope.filterEndDate = function(date){
    return $filter('date')(date, 'yyyy-MM-dd 23:59:59');
  };

  $scope.checkToday = function(date){
    var res = new Date(date) <= new Date()
    return res;
  }

}])

.controller('CheckerCtrl', function ($scope, localStorageService, $controller) {
  $scope.buttonAccess = function(task){
    var access =  false;
    var data = localStorageService.get('voucherData');
    angular.forEach(data.task, function(value, key){
      if(value.code.includes(task)){
       access = true;
       return access;
      }
    })
    return access;
  };
})

.controller('CommonCtrl', function ($scope, localStorageService, restService, $controller, rsc, code, authService) {
  $scope.parentCode = authService.getVoucherData().parentCode;
  var isSuperAdmin = $scope.parentCode == code.superadmin;
  $scope.getAllVoucherType = function(){
    var filter = {companyCode: $scope.parentCode};
    filter.sort ='label';
    restService.getVoucherResource(rsc.voucher_type + "/find-all").find(!isSuperAdmin ? filter : "").$promise.then(function (result) {
      $scope.list.voucherTypeList = result.content;
    });
  }

  $scope.getAllCompany = function(){
    var filter = {code: $scope.parentCode};
    filter.sort ='name';
    restService.getVoucherResource(rsc.company + "/find-all").find(!isSuperAdmin ? filter : "").$promise.then(function (result) {
      $scope.list.companyList = result.content;
    });
  }

});
