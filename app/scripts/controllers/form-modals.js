'use strict';

angular.module('promoApp')
  .controller('ModalsCtrl', function ($scope) {
    $scope.page = {
      title: 'Modals',
      subtitle: 'Place subtitle here...'
    };
  })

  .controller('ModalDemoCtrl', function ($scope, $modal, $log) {
    $scope.open = function(templateUrl, size, list) {
      var modalInstance = $modal.open({
        backdrop: 'static',
        size: size,
        templateUrl: templateUrl+'.html',
        controller: 'ModalInstanceCtrl',
        //size: size,
        resolve: {
          items: function () {
            return $scope.items;
          },
          list: function () {
            return $scope.list;
          },
          find: function () {
            return $scope.find;
          },
          save: function () {
            return $scope.save;
          },
          createItem: function(){
            return $scope.createItem;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        //$log.info('Modal dismissed at: ' + new Date());
      });
    };
    $scope.viewDetail = function(row) {
      var modalInstance = $modal.open({
        templateUrl: '_form-detail.html',
        controller: 'DetailCtrl',
        size: "lg",
        backdrop: 'static',
        resolve: {
          items: function () {
            return row;
          }
        }
      });
    }
  })

.controller('DetailCtrl', function ($scope, $modalInstance, items) {
  $scope.items = items;
  $scope.viewby = 10;
  $scope.currentPage = 1;
  $scope.itemsPerPage = $scope.viewby;
  $scope.maxSize = 10;

  $scope.setPage = function (pageNo) {
    $scope.currentPage = pageNo;
  };

  $scope.setItemsPerPage = function(num) {
    $scope.itemsPerPage = num;
    $scope.currentPage = 1; //reset to first page
  };

  $scope.getValueTrxType = function(val){
    var arr = val.split(" ");
    var res = arr.slice(-1)[0];
    return res;
  } 
  
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})

.controller('ModalInstanceCtrl', function ($scope, $modalInstance, items, $controller, list, save, createItem) {
  $controller('ToasterDemoCtrl', {$scope: $scope});
  $scope.available = false;
  $scope.cityAvailable = false;
  angular.isArray(items) ? $scope.items = items[0] : $scope.items = items;
  $scope.list = list;
  $scope.filter = {};

  $scope.selected = {
    item: $scope.items
  };

  $scope.addInput = function (name) {
    $scope.items[0][name].push("");
  };

  $scope.ok = function (items) {
    $scope.save(items);
    $modalInstance.close($scope.selected.item);
  };

  $scope.create_item = function (items) {
    if(items.password){
      var hashPassword = calculatePasswordHash(items.password, items.username);
      items.password = hashPassword;
    }
    $scope.createItem(items);
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $scope.items = [];
    $modalInstance.dismiss('cancel');
  };

  $scope.addInput = function(grp){
    $scope.items[0].grp.push({
      "id": ""
    });
  };

  function calculatePasswordHash(password, username) {
    var hashedPass = CryptoJS.SHA256(password.toString()).toString(CryptoJS.enc.Hex);
    var secret = CryptoJS.SHA256(username).toString(CryptoJS.enc.Hex);
    var hash = CryptoJS.HmacSHA256(hashedPass, secret);
    var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);
    return hashInBase64;
  };
  $scope.save = save;
  $scope.createItem = createItem;
})

  .controller('SplashDemoCtrl', function ($scope, $modal, $log) {
    $scope.items = ['item1', 'item2', 'item3'];

    $scope.openSplash = function(event, size) {

      var options = angular.element(event.target).data('options');

      var modalInstance = $modal.open({
        templateUrl: 'mySplashContent.html',
        controller: 'ModalInstanceCtrl',
        size: 'lg',
        backdropClass: 'splash' + ' ' + options,
        windowClass: 'splash' + ' ' + options,
        resolve: {
          items: function () {
            return $scope.items;
          }
        }
      });

      modalInstance.result.then(function (selectedItem) {
        $scope.selected = selectedItem;
      }, function () {
        $log.info('Modal dismissed at: ' + new Date());
      });
    };
  });

