'use strict';

var config = angular.module('promoApp');
config.constant('Msg', {
  'success': 'SUCCESS',
  'danger': 'DANGER',
  'error': 'ERROR',
  'info': 'INFO',
  'data_gagal_disimpan': 'Failed Data Stored !!!',
  'data_berhasil_disimpan': 'Data Saved Successfully',
  'data_gagal_diubah': 'Failed Data Changed !!!',
  'data_berhasil_diubah': 'Data Successfully Changed',
  'data_gagal_dihapus': 'Failed Data Deleted !!!',
  'data_berhasil_dihapus': 'Data Successfully Removed',
  'failed_get_data': "Failed to Get Data",
  'success_update_profile': 'Successfully Update Profile, please re-login'
});

config.constant('title', {
  'btn': {
    'edit': 'Edit',
    'save': 'Save'
  }
});

config.constant('field', {
  'extra' : 'extra',
  'extra1' : 'extra1',
  'extra1' : 'extra2',
  'extra1' : 'extra3',
  'txnReqDate': 'txnReqDate',
  'txnTypeType': 'txnTypeType',
  'pan' : 'pan',
  'maskedPan' : 'maskedPan',
  'companyCode' : 'companyCode',
  'tid' : 'tid',
  'mid' : 'mid',
  'invoice' : 'invoice',
  'originalAmount' : 'originalAmount',
  'salesAmount' : 'salesAmount',
  'voucherAmount' : 'voucherAmount',
  'responseCode' : 'responseCode',
  'responseDetail' : 'responseDetail',
  'additionalInfo' : 'additionalInfo'
});

config.constant('lable', {
  'no' : 'No',
  'view' : 'View',
  'transaction_date' : 'Transaction Date',
  'transaction_type' : 'Transaction Type',
  'token' : 'Tokenize PAN',
  'masking_pan' : 'Maked PAN',
  'company_code' : 'Company Code',
  'tid' : 'TID',
  'mid' : 'MID',
  'invoice_number' : 'Invoice Number',
  'original_amount' : 'Original Amount',
  'sales_amount' : 'Sales Amount',
  'voucher_amount' : 'Voucher Amount',
  'response_code' : 'Response Code',
  'response_detail' : 'Response Detail',
  'additional_info' : 'Additional Info'
});

config.constant('rsc', {
  'company': 'company',
  'merchant_group': 'merchantGroup',
  'group': 'group',
  'merchant': 'merchant',
  'promo': 'promo',
  'voucher': 'voucher',
  'voucher_generate': 'voucher/generate',
  'voucher_replace': 'voucher/replace',
  'parsing_voucher_generate': 'voucher-generate',
  'parsing_voucher_replace': 'voucher-replace',
  'voucher_generate_request': 'voucher-generate',
  'voucher_replace_request': 'voucher-replace',
  'voucher_type': 'voucher-type',
  'user': "user",
  'userfind': 'user/find',
  'rolefind': 'role/find',
  'role': 'role',
  'transaction_log': 'transaction-log',
  'voucher_detail': 'voucher-detail',
  'txn_log' : 'txn-log'
});

config.constant('ngAuthSettings', {
    apiServiceBaseUri: serviceBase,
    clientId: 'ngAuthApp'
});

config.constant('PROCESSOR', {
  'PROMO_POINT' : 'BonusPointProcessor',
  'PROMO_DISCOUNT_VALIDATION' : 'DiscountValidationProcessor',
  'PROMO_DIRECT_DISCOUNT' : 'DirectDiscountProcessor',
  'PROMO_FREE_GIFT' : 'FreeGiftProcessor',
  'PROMO_VOUCHER_GIFT' : 'GiftVoucherProcessor',
  'PROMO_DISCOUNT_CALCULATION' : 'DiscountCalculationProcessor',
  'PROMO_LUCKY_DRAW' : 'LuckyDrawProcessor',
});

config.constant('STATUS', {
  'BLOCK' : 0,
  'ACTIVATE' : 1,
  'REGISTER' : 2,
  'DELETE' : 3,
});

config.constant('txnTypes', [
  {id:1, name:"Generate"},
  {id:3, name:"Redeem"},
  {id:5, name:"Cancel"},
  {id:9, name:"Block"},
  {id:10, name:"Unblock"}
]);

config.constant('listStatus', { 
  'voucherTransfer' : {
    'status' : [
      {'code':1, 'name':'Draft'}, {'code':2, 'name':'Review'}, {'code':3, 'name':'Approved'}, {'code':4, 'name':'Rejected'}
    ]
   },
  'voucherDetail' : {
    'status' : [
      {'code':1, 'name':'Active'}, {'code':2, 'name':'Block'}, {'code':3, 'name':'Expired'}, {'code':4, 'name':'Used'}
    ]
  }
});

config.constant('code', {
  'superadmin' : 99999999999
});

config.constant('INFO', {
  'USER_MANAGEMENT_TITLE_INFO': 'USER MANAGEMENT INFO',
  'USER_CREATE_SUCCESS' : 'User created successfully',
  'USER_CREATE_FAILED'  : 'Failed to create user',
  'USER_UPDATED_SUCCESS': 'User updated successfully',
  'USER_UPDATE_FAILED'  : 'Failed to update user',
  'USER_DELETED_SUCCESS': 'User deleted successfully',
  'USER_DELETE_FAILED'  : 'Failed to delete user',
  'USER_RESET_SUCCESS'  : 'User password has been reset successfully',
  'USER_RESET_FAILED'   : 'Failed to reset user password',
  'CHANGE_PASSWORD_TITLE_INFO': 'CHANGE_PASSWORD INFO',
  'USER_CHANGE_PASSWORD_SUCCESS'  : 'Your password has been successfully changed',
  'USER_CHANGE_PASSWORD_FAILED'   : 'Failed to change your password',
  'USER_PASSWORD_NOT_MATCH'   : 'Old Password is not match',
  'USER_STATUS_DISABLE' : 'DISABLE',
  'ROLE_MANAGEMENT_TITLE_INFO' : 'ROLE MANAGEMENT INFO',
  'ROLE_CREATE_SUCCESS' : 'Role created successfully',
  'ROLE_CREATE_FAILED'  : 'Failed to create role',
  'ROLE_UPDATED_SUCCESS': 'Role updated successfully',
  'ROLE_UPDATE_FAILED'  : 'Failed to update role',
  'ROLE_DELETED_SUCCESS': 'Role deleted successfully',
  'ROLE_DELETE_FAILED'  : 'Failed to delete role',
  'NETWORK_INFO' : 'NETWORK INFO',
  'API_OFFLINE'  : 'Ooops something wrong with server. Please try again or contact your administrator',
  'MERCHANT_MANAGEMENT_TITLE_INFO': 'MERCHANT MANAGEMENT INFO',
  'PROMO_MANAGEMENT_TITLE_INFO': 'PROMO MANAGEMENT INFO',
  'MERCHANT_CREATE_SUCCESS' : 'Merchant created successfully',
  'MERCHANT_CREATE_FAILED'  : 'Failed to create Merchant',
  'MERCHANT_UPDATED_SUCCESS': 'Merchant updated successfully',
  'MERCHANT_UPDATE_FAILED'  : 'Failed to update Merchant',
  'MERCHANT_DELETED_SUCCESS': 'Merchant deleted successfully',
  'MERCHANT_DELETE_FAILED'  : 'Failed to delete Merchant',
  'MERCHANT_GROUP_CREATE_SUCCESS' : 'Merchant Group created successfully',
  'MERCHANT_GROUP_CREATE_FAILED'  : 'Failed to create Merchant Group',
  'MERCHANT_GROUP_UPDATED_SUCCESS': 'Merchant Group updated successfully',
  'MERCHANT_GROUP_UPDATE_FAILED'  : 'Failed to update Merchant Group',
  'MERCHANT_GROUP_DELETED_SUCCESS': 'Merchant Group deleted successfully',
  'MERCHANT_GROUP_DELETE_FAILED'  : 'Failed to delete Merchant Group',
  'COMPANY_CREATE_SUCCESS' : 'Company created successfully',
  'COMPANY_CREATE_FAILED'  : 'Failed to create Company',
  'COMPANY_UPDATED_SUCCESS': 'Company updated successfully',
  'COMPANY_UPDATE_FAILED'  : 'Failed to update Company',
  'COMPANY_DELETED_SUCCESS': 'Company deleted successfully',
  'COMPANY_DELETE_FAILED'  : 'Failed to delete Company',
  'GROUPS_CREATE_SUCCESS' : 'Group created successfully',
  'GROUPS_CREATE_FAILED'  : 'Failed to create Group',
  'GROUPS_UPDATED_SUCCESS': 'Group updated successfully',
  'GROUPS_UPDATE_FAILED'  : 'Failed to update Group',
  'GROUPS_DELETED_SUCCESS': 'Group deleted successfully',
  'GROUPS_DELETE_FAILED'  : 'Failed to delete Group',
  'PROMO_CREATE_SUCCESS' : 'Master Promo created successfully',
  'PROMO_CREATE_FAILED'  : 'Failed to create Master Promo',
  'PROMO_UPDATED_SUCCESS': 'Master Promo updated successfully',
  'PROMO_UPDATE_FAILED'  : 'Failed to update Master Promo',
  'PROMO_DELETED_SUCCESS': 'Master Promo deleted successfully',
  'PROMO_DELETE_FAILED'  : 'Failed to delete Master Promo',
  'REGISTER_PROMO_PROCESSOR_EMPTY'  : 'Processor Can`t Empty',
  'REGISTER_PROMO_SUCCESS'          : 'Create Campaign Successfully',
  'REGISTER_PROMO_FAILED'           : 'Create Campaign Failed',
  'UPDATE_PROMO_SUCCESS'            : 'Update Campaign Successfully',
  'UPDATE_PROMO_FAILED'             : 'Update Campaign Failed',
  'CHANGE_STATUS_PROMO_SUCCESS'     : 'Change Status Campaign Successfully',
  'CHANGE_STATUS_PROMO_FAILED'      : 'Change Status Campaign Failed',
  'REGISTER_PROMO_PROCESSOR_NOT_ALLOW' : 'Register Campaign Not Allow',
  'MANAGER_PROMO_GET_DATA_FAILED' : 'Failed to Get Promo Data',
  'MANAGER_PROMO_DELETE_FAILED'   : 'Failed to Delete Promo Data',
  'MANAGER_PROMO_DELETE_SUCCESS'  : 'Promo Data deleted successfully',
});