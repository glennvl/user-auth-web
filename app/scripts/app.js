'use strict';

angular.module('promoApp', [
    'pascalprecht.translate',
    'tmh.dynamicLocale',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'picardy.fontawesome',
    'ui.bootstrap',
    'ui.router',
    'ui.utils',
    'angular-loading-bar',
    'lazyModel',
    'toastr',
    'angularBootstrapNavTree',
    'oc.lazyLoad',
    'ui.select',
    'ui.tree',
    'textAngular',
    'angularFileUpload',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.edit',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.exporter',
    'easypiechart',
    'ui.calendar',
    'LocalStorageModule',
    'ui-notification',
    'ngLoadingSpinner',
    'ngCookies'
  ])
  .constant('LOCALES', {
    'locales': {
      'id': 'Indonesia',
      'en': 'English'
    },
    'preferredLocale': 'en'
  })
  .run(['$rootScope', '$state', '$stateParams', 'authService', '$location', '$modalStack', function($rootScope, $state, $stateParams, authService, $location, $modalStack) {
    authService.fillAuthData();
    if(authService.authentication.isAuth == false){
     $location.path('/page404');
    }
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.$on('$stateChangeSuccess', function(event, toState) {
      $modalStack.dismissAll();

      event.targetScope.$watch('$viewContentLoaded', function () {

        angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);

        setTimeout(function () {
          angular.element('#wrap').css('visibility','visible');

          if (!angular.element('.dropdown').hasClass('open')) {
            angular.element('.dropdown').find('>ul').slideUp();
          }
        }, 200);
      });
      $rootScope.containerClass = toState.containerClass;
    });
  }])

  .config(function ($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push('authInterceptorService');
  })
  .run(['authService', '$location', function (authService, $location) {
    authService.fillAuthData();
    if(authService.authentication.isAuth == false){
      $location.path('/page404');
    }
  }])

  .config(function ($translateProvider) {
    $translateProvider.useMissingTranslationHandlerLog();
  })

  .config(['$translateProvider', 'LOCALES' , function ($translateProvider, LOCALES) {
  $translateProvider.useStaticFilesLoader({
    prefix: 'resources/locale-',
    suffix: '.json'
  });
  $translateProvider.useCookieStorage();
  // $translateProvider.preferredLanguage('common');
  // $translateProvider.useSanitizeValueStrategy('escaped');
  $translateProvider.preferredLanguage(LOCALES.preferredLocale);
  $translateProvider.useLocalStorage();
  }])

  .config(function (tmhDynamicLocaleProvider) {
    tmhDynamicLocaleProvider.localeLocationPattern('bower_components/angular-i18n/angular-locale_{{locale}}.js');
  })

  .config(['uiSelectConfig', function (uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
  }])

  .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/login');

    $stateProvider

    .state('app', {
      abstract: true,
      url: '/app',
      templateUrl: 'views/app.html'
    })

    //app core pages (errors, login,signup)
    .state('core', {
      abstract: true,
      url: '/core',
      template: '<div ui-view></div>'
    })
    .state('login', {
      url: '/login',
      controller: 'LoginCtrl',
      templateUrl: 'views/tmpl/pages/login.html'
    })

    .state('app.dashboard', {
      url: '/dashboard',
      controller: 'DashboardCtrl',
      templateUrl: 'views/dashboard.html'
    })

    .state('app.company', {
      url: '/company',
      controller: 'CompanyCtrl',
      templateUrl: 'views/administration/company/index.html'
    })

    // .state('app.merchant-group', {
    //   url: '/merchant-group',
    //   controller: 'MerchantGroupCtrl',
    //   templateUrl: 'views/administration/merchant-group/index.html'
    // })

    // .state('app.promo-reward', {
    //   url: '/promo-reward',
    //   controller: 'PromoRewardCtrl',
    //   templateUrl: 'views/administration/promo-reward/index.html'
    // })

    // .state('app.list-campaign', {
    //   url: '/list-campaign',
    //   controller: 'ListCampaignCtrl',
    //   templateUrl: 'views/promo-campaign/list-campaign/index.html'
    // })

    // .state('app.create-campaign', {
    //   url: '/create-campaign',
    //   controller: 'CreateCampaignCtrl',
    //   templateUrl: 'views/promo-campaign/create-campaign/index.html'
    // })

    // .state('app.wizard', {
    //   url: '/wizard',
    //   controller: 'WizardCtrl',
    //   templateUrl: 'views/form-wizard.html'
    // })

    .state('app.transaction-log', {
      url: '/log',
      controller: 'TransactionLogCtrl',
      templateUrl: 'views/voucher/transaction-log/index.html'
    })

    .state('app.voucher-detail', {
      url: '/voucher-detail',
      controller: 'VoucherDetailCtrl',
      templateUrl: 'views/voucher/voucher-detail/index.html'
    })

    .state('app.voucher-generate', {
      url: '/voucher-generate',
      controller: 'VoucherGenerateCtrl',
      templateUrl: 'views/voucher/voucher-generate/index.html'
    })

    .state('app.voucher-replace', {
      url: '/voucher-replace',
      controller: 'VoucherReplaceCtrl',
      templateUrl: 'views/voucher/voucher-replace/index.html'
    })

    .state('app.voucher-type', {
      url: '/voucher-type',
      controller: 'VoucherTypeCtrl',
      templateUrl: 'views/administration/voucher-type/index.html'
    })

    .state('app.voucher-company', {
      url: '/voucher-company',
      controller: 'VoucherCompanyCtrl',
      templateUrl: 'views/administration/voucher-company/index.html'
    })

    .state('app.user', {
      url: '/user',
      controller: 'UserCtrl',
      templateUrl: 'views/administration/users/index.html'
    })

    .state('app.role', {
      url: '/role',
      controller: 'RoleCtrl',
      templateUrl: 'views/administration/roles/index.html'
    })

    .state('app.group', {
      url: '/group',
      controller: 'GroupCtrl',
      templateUrl: 'views/administration/groups/index.html'
    })

    //signup
    .state('core.signup', {
      url: '/signup',
      controller: 'SignupCtrl',
      templateUrl: 'views/tmpl/pages/signup.html'
    })
    //forgot password
    .state('core.forgotpass', {
      url: '/forgotpass',
      controller: 'ForgotPasswordCtrl',
      templateUrl: 'views/tmpl/pages/forgotpass.html'
    })
    //page 404
    .state('core.page404', {
      url: '/page404',
      templateUrl: 'views/tmpl/pages/page404.html'
    })
    //page 500
    .state('core.page500', {
      url: '/page500',
      templateUrl: 'views/tmpl/pages/page500.html'
    })
    //page offline
    .state('core.page-offline', {
      url: '/page-offline',
      templateUrl: 'views/tmpl/pages/page-offline.html'
    })
    //locked screen
    .state('core.expire', {
      url: '/expire',
      templateUrl: 'views/session-expire.html'
    })

  }]);
