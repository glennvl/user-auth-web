'use strict';

angular.module('promoApp')
  .directive('pageLoader', [
    '$timeout',
    function ($timeout) {
      return {
        restrict: 'AE',
        template: '<div class="dot1"></div><div class="dot2"></div>',
        link: function (scope, element, $http) {
          element.addClass('hide');
          scope.isLoading = function () {
            if(!$http.pendingRequests) return false;
            return $http.pendingRequests.length > 0;
          };
          scope.$watch(scope.isLoading, function (v)
          {
            if(v){
              $timeout(function () {
                      element.toggleClass('hide animate');
                    }, 600);
            }else{
              //element.toggleClass('hide animate');
            }
          });
          //scope.$on('$stateChangeStart', function() {
          //  element.toggleClass('hide animate');
          //});
          //scope.$on('$stateChangeSuccess', function (event) {
          //  event.targetScope.$watch('$viewContentLoaded', function () {
          //    $timeout(function () {
          //      element.toggleClass('hide animate');
          //    }, 600);
          //  });
          //});
        }
      };
    }
  ]);
