'use strict';

angular.module('promoApp')

.directive('price', function(numberFilter) {
    return {
        require: 'ngModel',
        link: function(scope, elm, attrs, ctrl) {
            ctrl.$formatters.unshift(function(modelValue) {
               return numberFilter(modelValue, 2);
            });
        }
    };
})

.directive('getStatus', function() {
    return {
      restrict: 'C',
      replace: true,
      transclude: true,
      scope: { myData: '@myData' },
      template: '<div ng-switch on="myData" style="text-align: center;">' +
                  '<div ng-switch-when=3 style="color:red;">Deleted</div>' +
                  '<div ng-switch-when=2 style="color:bg-success;">Registered</div>' +
                  '<div ng-switch-when=1 style="color:bg-success;">Activated</div>' +
                  '<div ng-switch-when=0 style="color:red;">Blocked</div>' +
                '</div>'
    };
})

.directive('getVoucherStatus', function() {
    return {
      restrict: 'C',
      replace: true,
      transclude: true,
      scope: { myData: '@myData' },
      template: '<div ng-switch on="myData" style="text-align: center;">' +
                  '<div ng-switch-when=1 class="label bg-greensea">Active</div>' +
                  '<div ng-switch-when=2 class="label bg-danger">Block</div>' +
                  '<div ng-switch-when=3 class="label bg-danger">Expire</div>' +
                  '<div ng-switch-when=4 class="label bg-danger">Used</div>' +
                '</div>'
    };
})

.directive('getVoucherGenerateStatus', function() {
    return {
      restrict: 'C',
      replace: true,
      transclude: true,
      scope: { myData: '@myData' },
      template: '<div ng-switch on="myData" style="text-align: center;">' +
                  '<div ng-switch-when=1 class="label bg-primary">Draft</div>' +
                  '<div ng-switch-when=2 class="label bg-primary">Review</div>' +
                  '<div ng-switch-when=3 class="label bg-greensea">Approved</div>' +
                  '<div ng-switch-when=4 class="label bg-danger">Rejected</div>' +
                '</div>'
    };
})

.directive('getTxnType', function() {
      return {
        restrict: 'C',
        replace: true,
        transclude: true,
        scope: { myData: '@myData' },
        template: '<div ng-switch on="myData" style="text-align: center;">' +
                    '<div ng-switch-when=Active>Active</div>' +
                    '<div ng-switch-when=Sales>Sales</div>' +
                    '<div ng-switch-when="Top Up">Top Up</div>' +
                    '<div ng-switch-when="Activation Conf">Active</div>' +
                    '<div ng-switch-when="Top Up Conf">Top Up</div>' +
                  '</div>'
      };

      // <td><span class="label" ng-class="{'bg-success' : order.status == 'sent','bg-warning' : order.status == 'closed','bg-lightred' : order.status == 'cancelled','bg-primary' : order.status == 'pending'}">{{ order.status }}</span></td>
})

.directive('ngRemoveClick', ['$modal',
    function($modal) {
      var ModalInstanceCtrl = function($scope, $modalInstance) {
        $scope.ok = function() {
          $modalInstance.close();
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      };

      return {
        restrict: 'A',
        scope: {
          ngRemoveClick:"&"
        },
        link: function(scope, element, attrs) {
          element.bind('click', function() {
            var message = attrs.ngRemoveMessage || "are you sure?";
            var title = attrs.ngRemoveTitle || "Delete";

            var modalHtml = '<div class="modal-header"><h3 class="modal-title custom-font" ><b>' + title + '</b></h3></div>';
                modalHtml += '<div class="modal-body">' + message + '</div>';
                modalHtml += '<div class="modal-footer"><button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c text-center" ng-click="ok()">Yes</button><button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c" ng-click="cancel()">No</button></div>';

            var modalInstance = $modal.open({
              template: modalHtml,
              controller: ModalInstanceCtrl
            });

            modalInstance.result.then(function() {
              scope.ngRemoveClick();
            }, function() {
              //Modal dismissed
            });
          });
        }
      };
    }
])

.directive('ngApprovedClick', ['$modal',
    function($modal) {
      var ModalInstanceCtrl = function($scope, $modalInstance) {
        $scope.ok = function() {
          $modalInstance.close();
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      };

      return {
        restrict: 'A',
        scope: {
          ngApprovedClick:"&"
        },
        link: function(scope, element, attrs) {
          element.bind('click', function() {
            var message = attrs.ngApprovedMessage || "are you sure?";
            var title = attrs.ngApprovedTitle;

            var modalHtml = '<div class="modal-header"><h3 class="modal-title custom-font" ><b>' + title + '</b></h3></div>';
                modalHtml += '<div class="modal-body">' + message + '</div>';
                modalHtml += '<div class="modal-footer"><button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c text-center" ng-click="ok()">Yes</button><button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c" ng-click="cancel()">No</button></div>';

            var modalInstance = $modal.open({
              template: modalHtml,
              controller: ModalInstanceCtrl
            });

            modalInstance.result.then(function() {
              scope.ngApprovedClick();
            }, function() {
              //Modal dismissed
            });
          });
        }
      };
    }
])

.directive('ngResetClick', ['$modal',
    function($modal) {

      var ModalInstanceCtrl = function($scope, $modalInstance) {
        $scope.ok = function() {
          $modalInstance.close();
        };

        $scope.cancel = function() {
          $modalInstance.dismiss('cancel');
        };
      };

      return {
        restrict: 'A',
        scope: {
          ngResetClick:"&"
        },
        link: function(scope, element, attrs) {
          element.bind('click', function() {
            var message = attrs.ngResetMessage || "are you sure?";

            var modalHtml = '<div class="modal-header"><h3 class="modal-title custom-font" ><b>Reset</b> Password</h3></div>';
                modalHtml += '<div class="modal-body">' + message + '</div>';
                modalHtml += '<div class="modal-footer"><button class="btn btn-lightred btn-ef btn-ef-4 btn-ef-4c" ng-click="cancel()">No</button><button class="btn btn-success btn-ef btn-ef-3 btn-ef-3c" ng-click="ok()">Yes</button></div>';

            var modalInstance = $modal.open({
              template: modalHtml,
              controller: ModalInstanceCtrl
            });

            modalInstance.result.then(function() {
              scope.ngResetClick();
            }, function() {
              //Modal dismissed
            });
          });
        }
      };
    }
])

.directive('showEmptyMsg', function ($compile, $timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var msg = (attrs.showEmptyMsg) ? attrs.showEmptyMsg : 'Nothing to display';
            var template = "<p style='text-align:center; margin-top:20px; opacity:0.8 z-index:999'>Nothing to display</p>";
            var tmpl = angular.element(template);
            $compile(tmpl)(scope);
            $timeout(function () {
                element.find('.uiViewPort').append(tmpl);
            }, 0);
        }
    };
})
.directive('displayCurrentTime', ['$interval', 'dateFilter',
  function($interval, dateFilter) {
    // return the directive link function. (compile function not needed)
    return function(scope, element, attrs) {
      var format,  // date format
        stopTime; // so that we can cancel the time updates

      // used to update the UI
      function updateTime() {
        element.text(dateFilter(new Date(), format));
      }

      // watch the expression, and update the UI on change.
      scope.$watch(attrs.myCurrentTime, function(value) {
        //format = value;
        format =  'dd-MM-yyyy hh:mm:ss';
        updateTime();
      });

      stopTime = $interval(updateTime, 1000);

      // listen on DOM destroy (removal) event, and cancel the next UI update
      // to prevent updating time after the DOM element was removed.
      element.on('$destroy', function() {
        $interval.cancel(stopTime);
      });
    }
  }])

.directive('nextFocus', function() {

  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      elem.bind('keydown', function(e) {
        var partsId = attrs.id.match( /\d+/);
        var currentId = parseInt(partsId);
        var code = e.keyCode || e.which;
        if (code === 13) {
          e.preventDefault();
          document.querySelector('#field' + (currentId + 1)).focus();
        }
      });
    }
  };
})

.directive('chosen', function($timeout) {
  var linker = function(scope, element) {
    scope.$watch('list.pot', function() {
      $timeout(function() {
        element.removeClass('loading');
        // element.attr('disabled', (element.attr('ng-disabled') ? attr.disabled : false));
        element.trigger('chosen:updated');
      }, 0, false);
    }, true);

    $timeout(function() {
      element.chosen();
    }, 0, false);
  };

  return {
    restrict: 'A',
    link: linker
  };
})
.directive('chosenUpdateList', function($timeout) {
  var linker = function(scope, element) {
    scope.$watch('list', function() {
      $timeout(function() {
        element.removeClass('loading');
        // element.attr('disabled', (element.attr('ng-disabled') ? attr.disabled : true));
        element.trigger('chosen:updated');
      }, 0, false);
    }, true);

    $timeout(function() {
      element.chosen();
    }, 0, false);
  };
  return {
    restrict: 'A',
    link: linker
  };
})
  //.directive('chosenUpdate', function($timeout) {
  //  var linker = function(scope, element) {
  //    scope.$watch('list', function() {
  //      $timeout(function() {
  //        element.trigger('chosen:updated');
  //      }, 0, false);
  //    }, true);
  //
  //    $timeout(function() {
  //      element.chosen();
  //      console.log("lalala");
  //    }, 0, false);
  //  };
  //
  //  return {
  //    require: 'ngModel',
  //    restrict: 'A',
  //    link: linker
  //  };
  //});
.directive('chosenUpdateItem', function($timeout) {
  var linker = function(scope, element) {
    scope.$watch('items', function() {
      $timeout(function() {
        element.trigger('chosen:updated');
      }, 0, false);
    }, true);

    $timeout(function() {
      element.chosen();
    }, 0, false);
  };
  return {
    restrict: 'A',
    link: linker
  };
})

.filter('currencyFilter', function () {
  return function (value, scope) {
    if(value == null || value == undefined || value == ""){
      return 0;
    }else{
      return value.toFixed(0).replace(/./g, function(c, i, a) {
        return i > 0 && c !== "." && (a.length - i) % 3 === 0 ? "." + c : c;
      });
    }
  };
})

.directive('datepickerStartdate', ['$parse', function ($parse) {
    var directive = {
        restrict: 'A',
        require: ['ngModel'],
        link: link
    };
    return directive;
    function link(scope, element, attr, ctrls) {
        var ngModelController = ctrls[0];
        ngModelController.$parsers.push(function (viewValue) {
            return formatStartDate(viewValue);
        });
    }
}])

.directive('datepickerEnddate', ['$parse', function ($parse) {
    var directive = {
        restrict: 'A',
        require: ['ngModel'],
        link: link
    };
    return directive;
    function link(scope, element, attr, ctrls) {
        var ngModelController = ctrls[0];
        ngModelController.$parsers.push(function (viewValue) {
            return formatEndDate(viewValue);
        });
    }
}])

.directive('format', ['$filter', function ($filter) {
    return {
        require: '?ngModel',
        link: function (scope, elem, attrs, ctrl) {
            if (!ctrl) return;
            ctrl.$formatters.unshift(function (a) {
                return $filter(attrs.format)(ctrl.$modelValue)
            });
            ctrl.$parsers.unshift(function (viewValue) {
                console.log(viewValue);
                if(viewValue){
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter('number')(plainNumber));
                    return plainNumber;
                }else{
                    return '';
                }
            });
        }
    };
}]);

function formatStartDate(date) {
  var strTime = "00:00:00";
  return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + strTime;
};
function formatEndDate(date) {
  var strTime = "23:59:59";
  return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate() + " " + strTime;
};
