'use strict';

angular.module('promoApp')
  .directive('collapseSidebar', function ($rootScope, localStorageService) {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {
        var app = angular.element('#minovate'),
            $window = angular.element(window),
            width = $window.width();

        var removeRipple = function() {
          angular.element('#sidebar').find('.ink').remove();
        };

        var collapse = function() {
          width = $window.width();
          var collapse_sidebar_object = localStorageService.get('collapse-sidebar')
          if (width < 992) {
            app.addClass('sidebar-sm');
          } else {
            app.removeClass('sidebar-sm sidebar-xs');
          }

          if (width < 768) {
            app.removeClass('sidebar-sm').addClass('sidebar-xs');
          } else if (width > 992){
            if(collapse_sidebar_object){
              app.addClass(collapse_sidebar_object.cols.toString());
            }else{
              app.removeClass('sidebar-sm sidebar-xs');
            };
          } else {
            app.removeClass('sidebar-xs').addClass('sidebar-sm');
          }

          if (app.hasClass('sidebar-sm-forced')) {
            app.addClass('sidebar-sm');
          }

          if (app.hasClass('sidebar-xs-forced')) {
            app.addClass('sidebar-xs');
          }

        };

        collapse();

        $window.resize(function() {
          if(width !== $window.width()) {
            var t;
            clearTimeout(t);
            t = setTimeout(collapse, 300);
            removeRipple();
          }
        });

        element.on('click', function(e) {
          if (app.hasClass('sidebar-sm')) {
            app.removeClass('sidebar-sm').addClass('sidebar-xs');
            localStorageService.set('collapse-sidebar', { cols: 'sidebar-xs' });
          }
          else if (app.hasClass('sidebar-xs')) {
            app.removeClass('sidebar-xs');
            localStorageService.set('collapse-sidebar', { cols: '' });
          }
          else {
            app.addClass('sidebar-sm');
            localStorageService.set('collapse-sidebar', { cols: 'sidebar-sm' });
          }

          app.removeClass('sidebar-sm-forced sidebar-xs-forced');
          app.parent().removeClass('sidebar-sm sidebar-xs');
          removeRipple();
          e.preventDefault();
        });

      }
    };
  });
