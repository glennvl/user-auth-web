angular.module('promoApp')
.directive('lowercase', function () {
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function (scope, element, attrs, ngModel) {
      
      //format text going to user (model to view)
      // ngModel.$formatters.push(function(value) {
      //   return value.toUpperCase();
      // });
      
      //format text from the user (view to model)
      ngModel.$parsers.push(function(value) {
        return value.toLowerCase();
      });
    }
  }
});